define(function() {

    function BeforePresentCalendar() {
        this.dayLength = 24 * 60 * 60;
        this.yearLength = 365 * this.dayLength + 97 * this.dayLength / 400; // Average year length in Gregorian Calendar
        this.UnixTimestampAtEpoch = -631152000; // gc.dateToTimestamp([1950, 1, 1])
    }

    BeforePresentCalendar.prototype.getUnits = function() {
        return [
            {
                name: "year",
                periodization: "decimal",
                indicativeLength: this.yearLength,
            },
        ];
    }

    BeforePresentCalendar.prototype.unitValueString = function(unit, value) {
        switch (unit) {
        case 0:
            return (-value).toString() + " B.P.";
        default:
            console.error("Undefined unit number for this calendar: %d", unit);
            return;
        }
    }

    BeforePresentCalendar.prototype.getValidRange = function() {
        return [undefined, this.UnixTimestampAtEpoch];
    }

    BeforePresentCalendar.prototype.timestampToDate = function(timestamp) {
        if (timestamp > this.UnixTimestampAtEpoch) {
            return undefined;
        }
        return [Math.floor((timestamp - this.UnixTimestampAtEpoch) / this.yearLength)];
    }

    BeforePresentCalendar.prototype.dateToTimestamp = function(date) {
        if (date[0] > 0) {
            return undefined;
        }
        return date[0] * this.yearLength + this.UnixTimestampAtEpoch;
    }

    BeforePresentCalendar.prototype.rectify = function(date) {
        if (date[0] > 0) {
            return [0];
        }
        return date;
    }

    return {
        BeforePresentCalendar: BeforePresentCalendar,
    };
});
