define(function() {

    function HebrewCalendar() {
        this.daysInMonth = [30, 29, 30, 29, 30, 30, 29,
                            30, 29, 30, 29, 30, 29];
        this.monthAbbrevs = ["Tishrei", "Cheshvan", "Kislev", "Tevet", "Shevat", "Adar I", "Adar II",
                             "Nisan", "Iyar", "Sivan", "Tammuz", "Av", "Elul"];

        // Normally lengths are in seconds
        this.dayLength = 24 * 60 * 60;
        this.UnixTimestampAtHebrewEpoch = -180799776000; // = jc.dateToTimestamp([-3761, 10, 7])

        // For molad calculations we use parts (halakim, 3+1/3 seconds) in order to stay with integers.
        this.partsPerHour = 1080;
        this.partsPerDay = 24 * this.partsPerHour;
        this.partsPerMonthlyMoladDiff = 29 * this.partsPerDay + 12 * this.partsPerHour + 793;

        // This array describes which Four Gate (0-3) each year within a 19-year cycle belongs to.
        this.fourGates = [3, 0, 2, 3, 0, 2, 3, 1, 3, 0, 2, 3, 0, 2, 3, 0, 2, 3, 1];

        this.tableOfLimits = [
            { minMolad: 0 * this.partsPerDay + 18 * this.partsPerHour +   0, types: ['D', 'D', 'D', 'D'] },
            { minMolad: 1 * this.partsPerDay +  9 * this.partsPerHour + 204, types: ['C', 'C', 'C', 'D'] },
            { minMolad: 1 * this.partsPerDay + 20 * this.partsPerHour + 491, types: ['C', 'C', 'C', 'C'] },
            { minMolad: 2 * this.partsPerDay + 15 * this.partsPerHour + 589, types: ['R', 'R', 'C', 'C'] },
            { minMolad: 2 * this.partsPerDay + 18 * this.partsPerHour +   0, types: ['R', 'R', 'R', 'R'] },
            { minMolad: 3 * this.partsPerDay +  9 * this.partsPerHour + 204, types: ['R', 'R', 'R', 'R'] },
            { minMolad: 3 * this.partsPerDay + 18 * this.partsPerHour +   0, types: ['R', 'R', 'R', 'D'] },
            { minMolad: 4 * this.partsPerDay + 11 * this.partsPerHour + 695, types: ['R', 'R', 'R', 'C'] },
            { minMolad: 5 * this.partsPerDay +  9 * this.partsPerHour + 204, types: ['C', 'C', 'C', 'C'] },
            { minMolad: 5 * this.partsPerDay + 18 * this.partsPerHour +   0, types: ['D', 'D', 'D', 'D'] },
            { minMolad: 6 * this.partsPerDay +  0 * this.partsPerHour + 408, types: ['C', 'D', 'D', 'D'] },
            { minMolad: 6 * this.partsPerDay +  9 * this.partsPerHour + 204, types: ['C', 'C', 'C', 'D'] },
            { minMolad: 6 * this.partsPerDay + 20 * this.partsPerHour + 491, types: ['C', 'C', 'C', 'C'] },
        ].sort((r1, r2) => (r2.minMolad - r1.minMolad)); // Sort in descending order.

        this.yearCache = new Map();

        this.isLeapYear = function(year) {
            return [0, 3, 6, 8, 11, 14, 17].includes(year % 19);
        }

        this.moladForYear = function(year) {
            var molad = (3*24 + 7) * this.partsPerHour + 695;

            molad += Math.floor(year / 19) * ((19 * 12 + 7) * this.partsPerMonthlyMoladDiff);

            for (var c = 0; c < (year % 19); c++) {
                if (this.isLeapYear(c))
                    molad += 13 * this.partsPerMonthlyMoladDiff;
                else
                    molad += 12 * this.partsPerMonthlyMoladDiff;
            }

            return molad;
        };

        this.getYearType = function(year) {
            var molad = this.moladForYear(year) % (7 * this.partsPerDay);

            for (let row of this.tableOfLimits) {
                if (molad >= row.minMolad) {
                    return row.types[this.fourGates[year % 19]];
                }
            }

            return this.tableOfLimits[0].types[this.fourGates[year % 19]];
        };

        this.getYearLength = function(year) {
            var days;

            switch (this.getYearType(year)) {
            case 'R': days = 354; break;
            case 'D': days = 353; break;
            case 'C': days = 355; break;
            default: console.error("Unknown year type"); return 0;
            }

            if (this.isLeapYear(year))
                days += 30;

            return days * this.dayLength;
        };

        this.getDaysInMonth = function(month, yearType) {
            if (yearType == 'D' && month == 3)
                return 29;
            if (yearType == 'C' && month == 2)
                return 30;
            return this.daysInMonth[month - 1];
        }
    }

    HebrewCalendar.prototype.getUnits = function() {
        return [
            {
                name: "year",
                periodization: "decimal",
                indicativeLength: 12,
            },
            {
                name: "month",
                periodization: "ternary",
                periodizationOffset: 1,
                indicativeLength: 30,
            },
            {
                name: "day",
                indicativeLength: this.dayLength,
                periodizationOffset: 1,
            },
        ];
    }

    HebrewCalendar.prototype.unitValueString = function(unit, value) {
        switch (unit) {
        case 0:
            return "AM " + value.toString();
        case 1:
            return this.monthAbbrevs[value - 1];
        case 2:
            return value.toString();
        default:
            console.error("Undefined unit number for this calendar: %d", unit);
            return;
        }
    }

    HebrewCalendar.prototype.getValidRange = function() {
        return [this.dateToTimestamp([1, 1, 1]), undefined];
    }

    HebrewCalendar.prototype.timestampToDate = function(timestamp) {
        var year = 1, month = 1, day;
        var s;
        var yearCache = this.yearCache;
        var accTimestamp = this.UnixTimestampAtHebrewEpoch;

        // Find latest timestamp in cache which is prior to desired timestamp.
        var bestCacheEntry = null;
        for (const [y, ts] of yearCache.entries()) {
            if ((bestCacheEntry === null || ts > yearCache.get(bestCacheEntry)) && ts < timestamp) {
                bestCacheEntry = y;
            }
        }
        if (bestCacheEntry !== null) {
            year = bestCacheEntry;
            accTimestamp = yearCache.get(bestCacheEntry);
            s -= yearCache.get(bestCacheEntry);
        }

        while (true) {
            var yLength = this.getYearLength(year);
            if (accTimestamp + yLength > timestamp)
                break;
            accTimestamp += yLength;
            year++;
            yearCache.set(year, accTimestamp);
        }

        s = timestamp - accTimestamp;

        var yType = this.getYearType(year);

        // Year calculation complete, find what day of that year we are in.
        day = Math.floor(s / this.dayLength) + 1;

        // See how many months we should pass, decrementing 'day' until it contains what day
        // in the current month we are in.
        for (var m = 0; m < 13; m++) {
            if (!this.isLeapYear(year) && m == 5) {
                month++;
                continue;
            }
            var daysInMonth = this.getDaysInMonth(m + 1, yType);

            if (day > daysInMonth) {
                month++;
                day -= daysInMonth;
            } else {
                break;
            }
        }

        if (year >= 1) {
            return [year, month, day];
        } else {
            return undefined;
        }
    }

    HebrewCalendar.prototype.dateToTimestamp = function(date) {
        var year;
        var s = this.UnixTimestampAtHebrewEpoch;
        var yType = this.getYearType(date[0]);
        var yearCache = this.yearCache;

        // Find timestamp at beginning of requested year.
        if (yearCache.has(date[0])) {
            // Exact cache hit, use as is.
            year = date[0];
            s = yearCache.get(year);
        } else {
            year = 1;
            var bestCacheEntry = null;
            for (const y of yearCache.keys()) {
                if ((bestCacheEntry === null || y > bestCacheEntry) && y < date[0]) {
                    bestCacheEntry = y;
                }
            }
            if (bestCacheEntry !== null) {
                // Desired year is not in cache, but we can start calculation from the latest year we have in the cache.
                year = bestCacheEntry;
                s = yearCache.get(bestCacheEntry);
            }
            for (; year < date[0]; year++) {
                yearCache.set(year, s);
                s += this.getYearLength(year);
            }
            yearCache.set(year, s);
        }

        // Include passed whole months in timestamp.
        for (var m = 0; m < date[1] - 1; m++) {
            if (!this.isLeapYear(year) && m == 5) {
                continue;
            }
            s += this.getDaysInMonth(m + 1, yType) * this.dayLength;
        }

        // Include remaining days in timestamp.
        s += (date[2] - 1) * this.dayLength;

        return s;
    }

    HebrewCalendar.prototype.rectify = function(date) {
        if (date === undefined) {
            return [1, 1, 1];
        }

        var year = date[0];
        var month = date[1];
        var day = date[2];

        if (day < 1) {
            day = 1;
        } else if (day > this.getDaysInMonth(month, this.getYearType(year))) {
            month++;
            day = 1;
        }

        if (month < 1) {
            month = day = 1;
        } else if (month > 13) {
            year++;
            month = day = 1;
        } else if (!this.isLeapYear(year) && month == 6) {
            month++;
            day = 1;
        }

        if (year <= 0) {
            return [1, 1, 1];
        }

        return [year, month, day];
    }

    return {
        HebrewCalendar: HebrewCalendar,
    };
});
