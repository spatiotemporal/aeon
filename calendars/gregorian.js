define(function() {

    const mod = (x, n) => (x % n + n) % n;
    const isLeapYear = (y) => ((y % 4 == 0) && !((y % 100 == 0) && (y % 400 != 0)))

    function GregorianCalendar() {
        this.daysInMonth = [31, 28, 31, 30, 31, 30,
                            31, 31, 30, 31, 30, 31];
        this.monthAbbrevs = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                             "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        // All lengths are in seconds
        this.dayLength = 24 * 60 * 60;
        this.normalYearLength = 365 * this.dayLength;
        this.leapYearLength = 366 * this.dayLength;
        this.fourHundredYearLength = 97 * this.leapYearLength + 303 * this.normalYearLength;
    }

    GregorianCalendar.prototype.getUnits = function() {
        return [
            {
                name: "year",
                periodization: "decimal",
                indicativeLength: 12,
            },
            {
                name: "month",
                periodization: "ternary",
                periodizationOffset: 1,
                indicativeLength: 30,
            },
            {
                name: "day",
                indicativeLength: this.dayLength,
                periodizationOffset: 1,
            },
        ];
    }

    GregorianCalendar.prototype.unitValueString = function(unit, value) {
        switch (unit) {
        case 0:
            if (value > 0) {
                return "AD " + value;
            } else {
                return (- value) + " BC";
            }
        case 1:
            return this.monthAbbrevs[value - 1];
        case 2:
            return value.toString();
        default:
            console.error("Undefined unit number for this calendar: %d", unit);
            return;
        }
    }

    GregorianCalendar.prototype.getValidRange = function() {
        return [undefined, undefined];
    }

    GregorianCalendar.prototype.timestampToDate = function(timestamp) {
        // Start counting from year 2000 since it's the first four-hundred-year period after
        // the UNIX epoch (1970).
        var year = 2000, month = 1, day;
        var s = timestamp - 23 * this.normalYearLength - 7 * this.leapYearLength;

        // A period of four hundred consecutive years always has the same total length
        // (97 leap years and 303 normal years), so start by finding out how many such
        // periods we can fit.
        year += 400 * Math.floor(s / (this.fourHundredYearLength));
        s = mod(s, this.fourHundredYearLength);

        if (s < (25 * this.leapYearLength + 75 * this.normalYearLength)) {
            // See how many whole four-year periods are left.
            year += 4 * Math.floor(s / (this.leapYearLength + 3 * this.normalYearLength));
            s = mod(s, this.leapYearLength + 3 * this.normalYearLength);
            // See if we should pass the next year, which must be a leap year.
            if (s >= this.leapYearLength) {
                year++;
                s -= this.leapYearLength;
                // See how many normal years we should move past.
                year += Math.floor(s / this.normalYearLength);
                s = mod(s, this.normalYearLength);
            }
        } else {
            // Move past the first century which has 25 leap years.
            year += 100;
            s -= (25 * this.leapYearLength + 75 * this.normalYearLength);

            // See how many centuries we can move past, each of these having 24 leap years.
            year += 100 * Math.floor(s / (76 * this.normalYearLength + 24 * this.leapYearLength));
            s = mod(s, 76 * this.normalYearLength + 24 * this.leapYearLength);

            if (s < 4 * this.normalYearLength) {
                year += Math.floor(s / this.normalYearLength);
                s = mod(s, this.normalYearLength);
            } else {
                year += 4;
                s -= 4 * this.normalYearLength;

                // See how many whole four-year periods are left.
                year += 4 * Math.floor(s / (this.leapYearLength + 3 * this.normalYearLength));
                s = mod(s, this.leapYearLength + 3 * this.normalYearLength);
                // See if we should pass the next year, which must be a leap year.
                if (s >= this.leapYearLength) {
                    year++;
                    s -= this.leapYearLength;
                    // See how many normal years we should move past.
                    year += Math.floor(s / this.normalYearLength);
                    s = mod(s, this.normalYearLength);
                }
            }
        }

        // Year calculation complete, find what day of that year we are in.
        day = Math.floor(s / this.dayLength) + 1;

        // See how many months we should pass, decrementing 'day' until it contains what day
        // in the current month we are in.
        for (var m = 0; m < 12; m++) {
            var daysInMonth = this.daysInMonth[m];
            if (isLeapYear(year) && m == 1) {
                daysInMonth++;
            }
            if (day > daysInMonth) {
                month++;
                day -= daysInMonth;
            } else {
                break;
            }
        }

        if (year <= 0) {
            year--;
        }

        return [year, month, day];
    }

    GregorianCalendar.prototype.dateToTimestamp = function(date) {
        var year = date[0];
        var month = date[1];
        var day = date[2];
        // UNIX timestamp for 2000.
        var s = 23 * this.normalYearLength + 7 * this.leapYearLength;

        if (year < 0) {
            year++
        }

        // See how many four-hundred-year periods have passed since 1972.
        s += Math.floor((year - 2000) / 400) * this.fourHundredYearLength
        var yearsLeft = mod(year - 2000, 400);

        if (yearsLeft < 100) {
            // See how many four-year periods are left.
            s += Math.floor((yearsLeft) / 4) * (this.leapYearLength + 3 * this.normalYearLength);
            yearsLeft = mod(yearsLeft, 4);

            // Any remaining years start with a leap year followed by up to two normal years.
            if (yearsLeft >= 1) {
                s += this.leapYearLength;
                yearsLeft--;
            }
            s += yearsLeft * this.normalYearLength;
        } else {
            // The first century has 25 leap years.
            s += 75 * this.normalYearLength + 25 * this.leapYearLength;
            yearsLeft -= 100;

            // See how many centuries are left.
            s += Math.floor((yearsLeft) / 100) * (76 * this.normalYearLength + 24 * this.leapYearLength);
            yearsLeft = mod(yearsLeft, 100);

            if (yearsLeft < 4) {
                s += yearsLeft * this.normalYearLength;
            } else {
                s += 4 * this.normalYearLength;
                yearsLeft -= 4;

                // See how many four-year periods are left.
                s += Math.floor((yearsLeft) / 4) * (this.leapYearLength + 3 * this.normalYearLength);
                yearsLeft = mod(yearsLeft, 4);

                // Any remaining years start with a leap year followed by up to two normal years.
                var yearsLeft = mod(yearsLeft, 4);
                if (yearsLeft >= 1) {
                    s += this.leapYearLength;
                    yearsLeft--;
                }
                s += yearsLeft * this.normalYearLength;
            }
        }


        // Include passed whole months in timestamp.
        for (var m = 0; m < month - 1; m++) {
            s += this.daysInMonth[m] * this.dayLength;
        }
        if (isLeapYear(year) && month > 2) {
            s += this.dayLength;
        }

        // Include remaining days in timestamp.
        s += (day - 1) * this.dayLength;

        return s;
    }

    GregorianCalendar.prototype.rectify = function(date) {
        var year = date[0];
        var month = date[1];
        var day = date[2];

        if (month < 1) {
            month = day = 1;
        } else if (month > 12) {
            year++;
            month = day = 1;
        }

        if (year == 0) {
            return [1, 1, 1];
        }

        var daysInMonth = this.daysInMonth[month - 1];
        if (isLeapYear(year) == 0 && month == 2) {
            daysInMonth++;
        }
        if (day < 1) {
            day = 1;
        } else if (day > daysInMonth) {
            month++;
            day = 1;
        }

        if (month > 12) {
            year++;
            month = day = 1;
        }

        if (year == 0) {
            return [1, 1, 1];
        }

        return [year, month, day];
    }

    return {
        GregorianCalendar: GregorianCalendar,
    };
});
