// First Sothic cycle:
// 19 July 2781 BC

define(function() {

    const mod = (x, n) => (x % n + n) % n;

    function AncientEgyptianCalendar() {
        this.monthAbbrevs = ["I Akhet", "II Akhet", "III Akhet", "IV Akhet",
                             "I Peret", "II Peret", "III Peret", "IV Peret",
                             "I Shemu", "II Shemu", "III Shemu", "IV Shemu",
                             "intercalation"];
        // All lengths are in seconds
        this.dayLength = 24 * 60 * 60;
        this.monthLength = 30 * this.dayLength;
        this.yearLength = 365 * this.dayLength;
        this.sothicCycleLength = 1461 * this.yearLength;
        this.offsetFromFirstSothicCycleAtUnixEpoch = 149878252800;
    }

    AncientEgyptianCalendar.prototype.getUnits = function() {
        return [
            {
                name: "Sothic cycle",
                periodization: "decimal",
                indicativeLength: 1461,
            },
            {
                name: "year",
                periodization: "decimal",
                indicativeLength: 12,
            },
            {
                name: "month",
                periodization: "ternary",
                indicativeLength: 30,
            },
            {
                name: "day",
                periodization: "",
                indicativeLength: this.dayLength,
            },
        ];
    }

    AncientEgyptianCalendar.prototype.unitValueString = function(unit, value) {
        switch (unit) {
        case 0:
            return "S.C. " + value;
        case 1:
            return "yr " + value;
        case 2:
            return this.monthAbbrevs[value - 1];
        case 3:
            return value.toString();
        default:
            console.error("Undefined unit number for this calendar: %d", unit);
            return;
        }
    }

    AncientEgyptianCalendar.prototype.getValidRange = function() {
        return [undefined, undefined];
    }

    AncientEgyptianCalendar.prototype.timestampToDate = function(timestamp) {
        var s = timestamp + this.offsetFromFirstSothicCycleAtUnixEpoch;
        var sothicCycle = 1, year = 1, month = 1, day = 1;

        sothicCycle += Math.floor(s / this.sothicCycleLength);
        s = mod(s, this.sothicCycleLength);

        year += Math.floor(s / this.yearLength);
        s = mod(s, this.yearLength);

        month += Math.floor(s / this.monthLength);
        s = mod(s, this.monthLength);

        day += Math.floor(s / this.dayLength);

        return [sothicCycle, year, month, day];
    }

    AncientEgyptianCalendar.prototype.dateToTimestamp = function(date) {
        return -this.offsetFromFirstSothicCycleAtUnixEpoch
            + (date[0] - 1) * this.sothicCycleLength
            + (date[1] - 1) * this.yearLength
            + (date[2] - 1) * this.monthLength
            + (date[3] - 1) * this.dayLength;
    }

    AncientEgyptianCalendar.prototype.rectify = function(date) {
        var sothicCycle = date[0];
        var year = date[1];
        var month = date[2];
        var day = date[3];

        if (day > (month > 12 ? 5 : 30)) {
            month++;
            day = 1;
        }

        if (month > 13) {
            year++;
            month = day = 1;
        }

        if (year > 1461) {
            sothicCycle++;
            year = month = day = 1;
        }

        return [sothicCycle, year, month, day];
    }

    return {
        AncientEgyptianCalendar: AncientEgyptianCalendar,
    };
});
