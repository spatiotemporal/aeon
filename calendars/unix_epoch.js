define(function() {

    function UnixEpochCalendar() {
    }

    UnixEpochCalendar.prototype.getUnits = function() {
        return [
            {
                name: "second",
                periodization: "decimal",
                indicativeLength: 1,
            },
        ];
    }

    UnixEpochCalendar.prototype.unitValueString = function(unit, value) {
        switch (unit) {
        case 0:
            return value.toString();
        default:
            console.error("Undefined unit number for this calendar: %d", unit);
            return;
        }
    }

    UnixEpochCalendar.prototype.getValidRange = function() {
        return [undefined, undefined];
    }

    UnixEpochCalendar.prototype.timestampToDate = function(timestamp) {
        return [timestamp];
    }

    UnixEpochCalendar.prototype.dateToTimestamp = function(date) {
        return date[0];
    }

    UnixEpochCalendar.prototype.rectify = function(date) {
        return date;
    }

    return {
        UnixEpochCalendar: UnixEpochCalendar,
    };
});
