define(function() {

    // This calendar implements the ISO 8601 week date calendar, which
    // moves the new year to always be on a Monday. The implementation
    // is done rather lazily on top of the Gregorian calendar
    // implementation and can certainly be optimized. Algorithms are
    // adapted from https://en.wikipedia.org/wiki/ISO_week_date

    const div = (x, n) => (Math.floor(x / n));
    const mod = (x, n) => (x % n + n) % n;
    const isLeapYear = (y) => ((y % 4 == 0) && !((y % 100 == 0) && (y % 400 != 0)));
    const daysInYear = (y) => (isLeapYear(y) ? 366 : 365);

    function GregorianWeekdayCalendar() {
        this.daysInMonth = [31, 28, 31, 30, 31, 30,
                            31, 31, 30, 31, 30, 31];
        this.dayAbbrevs = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
        this.dayLength = 24 * 60 * 60;

        this.timestampToDayOfWeek = function(timestamp) {
            return (Math.floor(timestamp / this.dayLength) + 3) % 7 + 1;
        }

        this.weeksInYear = function(year) {
            const p = (y) => (mod(y + div(y, 4) - div(y, 100) + div(y, 400), 7));
            return (p(year) == 4 || p(year - 1) == 3) ? 53 : 52;
        }
    }

    GregorianWeekdayCalendar.prototype.getUnits = function() {
        return [
            {
                name: "year",
                periodization: "decimal",
                indicativeLength: 52,
            },
            {
                name: "week",
                periodization: "decimal",
                indicativeLength: 7,
            },
            {
                name: "day",
                indicativeLength: this.dayLength,
                periodizationOffset: 1,
            },
        ];
    }

    GregorianWeekdayCalendar.prototype.unitValueString = function(unit, value) {
        switch (unit) {
        case 0:
            if (value > 0) {
                return "AD " + value;
            } else {
                return (- value) + " BC";
            }
        case 1:
            return "w. " + value.toString();
        case 2:
            return this.dayAbbrevs[value - 1];
        default:
            console.error("Undefined unit number for this calendar: %d", unit);
            return;
        }
    }

    GregorianWeekdayCalendar.prototype.getValidRange = function() {
        return [undefined, undefined];
    }

    GregorianWeekdayCalendar.prototype.timestampToDate = function(timestamp) {
        var date = gc.timestampToDate(timestamp);
        var dow = this.timestampToDayOfWeek(timestamp);
        var doy = Math.floor((timestamp - gc.dateToTimestamp([date[0], 1, 1])) / this.dayLength);
        var woy = Math.floor((10 + doy - dow) / 7);
        var year = date[0];

        if (woy == 0) {
            year--;
            woy = this.weeksInYear(year);
        } else if (woy > this.weeksInYear(year)) {
            year++;
            woy = 1;
        }

        return [year, woy, dow];
    }

    GregorianWeekdayCalendar.prototype.dateToTimestamp = function(date) {
        var year = date[0], month = 1;
        var day = 7 * date[1] + date[2]
            - (this.timestampToDayOfWeek(gc.dateToTimestamp([date[0], 1, 4])) + 3);

        if (day < 1) {
            day += daysInYear(year - 1);
            year--;
        } else if (day > daysInYear(year)) {
            day -= daysInYear(year);
            year++;
        }

        for (var m = 0; m < 12; m++) {
            var daysInMonth = this.daysInMonth[m];
            if (isLeapYear(year) && m == 1) {
                daysInMonth++;
            }
            if (day > daysInMonth) {
                month++;
                day -= daysInMonth;
            } else {
                break;
            }
        }

        return gc.dateToTimestamp([year, month, day]);
    }

    GregorianWeekdayCalendar.prototype.rectify = function(date) {
        var year = date[0];
        var week = date[1];
        var day = date[2];

        if (day < 1) {
            day = 1;
        } else if (day > 7) {
            week++;
            day = 1;
        }

        if (week < 1) {
            week = day = 1;
        } else if (week > this.weeksInYear(year)) {
            year++;
            week = day = 1;
        }

        if (year == 0) {
            return [1, 1, 1];
        }

        return [year, week, day];
    }

    return {
        GregorianWeekdayCalendar: GregorianWeekdayCalendar,
    };
});
