define(['require', '../calendars/julian', '../calendars/gregorian'], function(require) {

    var julian = require('../calendars/julian');
    var gregorian = require('../calendars/gregorian');

    function AstronomicalYearCalendar() {
        this.daysInMonth = [31, 28, 31, 30, 31, 30,
                            31, 31, 30, 31, 30, 31];
        this.monthAbbrevs = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                             "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        this.jc = new julian.JulianCalendar();
        this.gc = new gregorian.GregorianCalendar();
        this.dayLength = 24 * 60 * 60;
        this.transitionYear = 1582;
        this.transitionTimestamp = this.gc.dateToTimestamp([this.transitionYear, 1, 1]);
    }

    AstronomicalYearCalendar.prototype.getUnits = function() {
        return [
            {
                name: "year",
                periodization: "decimal",
                indicativeLength: 12,
            },
            {
                name: "month",
                periodization: "ternary",
                periodizationOffset: 1,
                indicativeLength: 30,
            },
            {
                name: "day",
                indicativeLength: this.dayLength,
                periodizationOffset: 1,
            },
        ];
    }

    AstronomicalYearCalendar.prototype.unitValueString = function(unit, value) {
        switch (unit) {
        case 0:
            if (value > 0) {
                return "+" + value.toString();
            } else {
                return value.toString();
            }
        case 1:
            return this.monthAbbrevs[value - 1];
        case 2:
            return value.toString();
        default:
            console.error("Undefined unit number for this calendar: %d", unit);
            return;
        }
    }

    AstronomicalYearCalendar.prototype.getValidRange = function() {
        return [undefined, undefined];
    }

    AstronomicalYearCalendar.prototype.timestampToDate = function(timestamp) {
        if (timestamp >= this.transitionTimestamp) {
            return this.gc.timestampToDate(timestamp);
        } else {
            var date = this.jc.timestampToDate(timestamp);
            if (date[0] < 0) {
                date[0] += 1;
            }
            return date;
        }
    }

    AstronomicalYearCalendar.prototype.dateToTimestamp = function(date) {
        if (date[0] >= this.transitionYear) {
            return this.gc.dateToTimestamp(date);
        } else if (date[0] > 0) {
                return this.jc.dateToTimestamp(date);
        } else {
            return this.jc.dateToTimestamp([date[0] - 1, date[1], date[2]]);
        }
    }

    AstronomicalYearCalendar.prototype.rectify = function(date) {
        if (date[0] >= this.transitionYear) {
            return this.gc.rectify(date);
        } else {
            var year = date[0];
            var month = date[1];
            var day = date[2];

            if (month < 1) {
                month = day = 1;
            } else if (month > 12) {
                year++;
                month = day = 1;
            }

            var daysInMonth = this.daysInMonth[month - 1];
            if (year % 4 == 0 && month == 2) {
                daysInMonth++;
            }
            if (day < 1) {
                day = 1;
            } else if (day > daysInMonth) {
                month++;
                day = 1;
            }

            if (month > 12) {
                year++;
                month = day = 1;
            }

            return [year, month, day];
        }
    }

    return {
        AstronomicalYearCalendar: AstronomicalYearCalendar,
    };
});
