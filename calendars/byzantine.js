define(function() {

    const mod = (x, n) => (x % n + n) % n;

    function ByzantineCalendar() {
        this.daysInMonth = [30, 31, 30, 31, 31, 28,
                            31, 30, 31, 30, 31, 31];
        this.monthAbbrevs = ["Sep", "Oct", "Nov", "Dec", "Jan", "Feb",
                             "Mar", "Apr", "May", "Jun", "Jul", "Aug"];
        // All lengths are in seconds
        this.dayLength = 24 * 60 * 60;
        this.normalYearLength = 365 * this.dayLength;
        this.leapYearLength = 366 * this.dayLength;
        this.fourYearLength = this.leapYearLength + 3 * this.normalYearLength;
        this.offsetFromGregorianAtUnixEpoch = -13 * this.dayLength + 122 * this.dayLength;
        this.firstLeapYearAfterUnixEpoch = 5508 + 1972;
    }

    ByzantineCalendar.prototype.getUnits = function() {
        return [
            {
                name: "year",
                periodization: "decimal",
                indicativeLength: 12,
            },
            {
                name: "month",
                periodization: "ternary",
                periodizationOffset: 1,
                indicativeLength: 30,
            },
            {
                name: "day",
                indicativeLength: this.dayLength,
                periodizationOffset: 1,
            },
        ];
    }

    ByzantineCalendar.prototype.unitValueString = function(unit, value) {
        switch (unit) {
        case 0:
            return value + " AM";
        case 1:
            return this.monthAbbrevs[value - 1];
        case 2:
            return value.toString();
        default:
            console.error("Undefined unit number for this calendar: %d", unit);
            return;
        }
    }

    ByzantineCalendar.prototype.getValidRange = function() {
        return [this.dateToTimestamp([1, 1, 1]), undefined];
    }

    ByzantineCalendar.prototype.timestampToDate = function(timestamp) {
        // Start counting from the first leap year after the UNIX epoch (1970).
        var year = this.firstLeapYearAfterUnixEpoch, month = 1, day;
        var s = timestamp + this.offsetFromGregorianAtUnixEpoch - 2 * this.normalYearLength;

        // A period of four consecutive years always has the same total length (one leap
        // year and 3 normal years), so start by finding out how many such periods we can
        // fit.
        year += 4 * Math.floor(s / this.fourYearLength);
        s = mod(s, this.fourYearLength);
        // See if we should pass the next year, which must be a leap year.
        if (s >= this.leapYearLength) {
            year++;
            s -= this.leapYearLength;
            // See how many normal years we should move past.
            year += Math.floor(s / this.normalYearLength);
            s = mod(s, this.normalYearLength);
        }

        // Year calculation complete, find what day of that year we are in.
        day = Math.floor(s / this.dayLength) + 1;

        // See how many months we should pass, decrementing 'day' until it contains what day
        // in the current month we are in.
        for (var m = 0; m < 12; m++) {
            var daysInMonth = this.daysInMonth[m];
            if (year % 4 == 0 && m == 5) {
                daysInMonth++;
            }
            if (day > daysInMonth) {
                month++;
                day -= daysInMonth;
            } else {
                break;
            }
        }

        if (year >= 1) {
            return [year, month, day];
        } else {
            return undefined;
        }
    }

    ByzantineCalendar.prototype.dateToTimestamp = function(date) {
        var year = date[0];
        var month = date[1];
        var day = date[2];
        // UNIX timestamp for first leap year after Unix epoch.
        var s = 2 * this.normalYearLength;

        // See how many four-year periods have passed.
        s += Math.floor((year - this.firstLeapYearAfterUnixEpoch) / 4) * this.fourYearLength;

        // Any remaining years start with a leap year followed by up to two normal years.
        var yearsLeft = mod(year - this.firstLeapYearAfterUnixEpoch, 4);
        if (yearsLeft >= 1) {
            s += this.leapYearLength;
            yearsLeft--;
        }
        s += yearsLeft * this.normalYearLength;

        // Include passed whole months in timestamp.
        for (var m = 0; m < month - 1; m++) {
            s += this.daysInMonth[m] * this.dayLength;
        }
        if (year % 4 == 0 && month > 6) {
            s += this.dayLength;
        }

        // Include remaining days in timestamp.
        s += (day - 1) * this.dayLength;

        return s - this.offsetFromGregorianAtUnixEpoch;
    }

    ByzantineCalendar.prototype.rectify = function(date) {
        if (date === undefined) {
            return [1, 1, 1];
        }

        var year = date[0];
        var month = date[1];
        var day = date[2];

        if (month < 1) {
            month = day = 1;
        } else if (month > 12) {
            year++;
            month = day = 1;
        }

        if (year <= 0) {
            return [1, 1, 1];
        }

        var daysInMonth = this.daysInMonth[month - 1];
        if (year % 4 == 0 && month == 6) {
            daysInMonth++;
        }
        if (day < 1) {
            day = 1;
        } else if (day > daysInMonth) {
            month++;
            day = 1;
        }

        if (month > 12) {
            year++;
            month = day = 1;
        }

        return [year, month, day];
    }

    return {
        ByzantineCalendar: ByzantineCalendar,
    };
});
