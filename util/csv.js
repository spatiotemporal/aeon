define(function() {

    function parse(csv, headerRow = null, firstDataRow = 0) {
        var csvRows = csv.split("\n").filter(function(row) { return row != ""; });
        var rows = csvRows.slice(firstDataRow).map(function(row) {
            return row.split(",");
        });
        if (headerRow === null) {
            return rows;
        } else {
            var headings = csvRows[headerRow].split(",");
            return { headings: headings, data: rows };
        }
    }

    return {
        parse: parse,
    };

});
