define(function() {

    function get(url, headers = {}, timeout = null) {
        return new Promise(function(resolve, reject) {

            var request = new XMLHttpRequest();
            request.onloadend = function() {
                if (this.readyState == 4 && this.status == 200) {
                    resolve(request.responseText);
                } else {
                    reject();
                }
            };

            request.open("GET", url, true);
            if (timeout !== null) {
                request.timeout = timeout;
            }
            for (let [hdr, value] of Object.entries(headers)) {
                request.setRequestHeader(hdr, value);
            }
            request.send();
        });
    }

    function getXML(url, headers = {}, timeout = null) {
        return new Promise(function(resolve, reject) {

            var request = new XMLHttpRequest();
            request.onloadend = function() {
                if (this.readyState == 4 && this.status == 200) {
                    resolve(request.responseXML);
                } else {
                    reject();
                }
            };

            request.open("GET", url, true);
            if (timeout !== null) {
                request.timeout = timeout;
            }
            for (let [hdr, value] of Object.entries(headers)) {
                request.setRequestHeader(hdr, value);
            }
            request.send();
        });
    }

    return {
        get: get,
        getXML: getXML,
    };

});
