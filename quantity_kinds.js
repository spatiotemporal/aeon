define(function() {

    const QUANTITY_KINDS = {
        "count": "each",
        "ratio": "1",
        "angle": "rad",
        "length": "m",
        "time": "s",
        "speed": "m/s",
        "temperature": "tempC",
        "temperature_difference": "degC",
        "degree_days": "degK*s",
        "volumetric_flux": "m/s",
        "pressure": "Pa",
        "concentration": "mol/m^3",
        "body_mass_index": "kg/m^2",
    };

    return {
        QUANTITY_KINDS: QUANTITY_KINDS,
    };

});
