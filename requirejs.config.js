({
    baseUrl: ".",
    paths: {
        "d3": "node_modules/d3/dist/d3",
        "d3-xyzoom": "d3-xyzoom/build/d3-xyzoom",
    },
    include: ["node_modules/almond/almond", "d3-selection", "d3-transition", "d3-interpolate", "d3-drag", "d3-dispatch"],
    exclude: ["d3"],
    name: "aeon",
    out: "build/aeon.full.js",
    wrap: {
        startFile: "requirejs.startfile.js",
        endFile: "requirejs.endfile.js"
    },
    optimize: "none"
})
