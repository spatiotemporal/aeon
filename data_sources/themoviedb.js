define(['require', '../util/http', '../calendars/gregorian'], function(require) {

    var http = require('../util/http');
    var gregorian = require('../calendars/gregorian');

    const BASE_URL = "https://api.themoviedb.org/3";
    const MOVIE_PAGE_BASE_URL = "https://www.themoviedb.org/movie/";
    const DEFAULT_MAX_PAGES = 3;

    var serverConfig = null;

    function TheMovieDBDataset() {
        var self = this;
        this.token = null;
        this.genres = null;
        this.people = null;
        this.maxPages = DEFAULT_MAX_PAGES;

        this.get = function(request) {
            return http.get(BASE_URL + request, {
                "Authorization": "Bearer " + self.token,
            });
        };

        this.fetchPage = function(resolve, reject, baseUrl, page, maxPages, data) {
            var request = baseUrl + "&page=" + page;
            self.get(request).then(function(json) {
                var resp = JSON.parse(json);
                var collectedData = data.concat(resp.results)
                if (page >= resp.total_pages || page >= maxPages) {
                    var a = {
                        type: 'entity',
                        dataset: self,
                    };
                    a.data = collectedData.filter(function(r) {
                        return !!r.title && !!r.release_date;
                    }).map(function(r) {
                        var ts = self.gregorianCalendar.dateToTimestamp(r.release_date.split("-"));
                        return {
                            id: r.id,
                            title: r.title,
                            description: r.overview,
                            links: [{ resource: "The Movie DB", url: MOVIE_PAGE_BASE_URL + r.id }],
                            start: ts,
                            end: ts,
                            image: serverConfig.imageBaseURL + serverConfig.imageSize + r.poster_path,
                        };
                    });
                    resolve(a);
                } else {
                    self.fetchPage(resolve, reject, baseUrl, page + 1, maxPages, collectedData);
                }
            });
        };

        this.fetchMovies = function(resolve, reject) {
            var self = this;
            var request = "/discover/movie?sort_by=popularity.desc";
            if (self.genres !== null) {
                request += "&with_genres=" + self.genres.join(",");
            }
            if (self.people !== null) {
                request += "&with_people=" + self.people.join(",");
            }
            self.fetchPage(resolve, reject, request, 1, this.maxPages, []);
        };
    };

    TheMovieDBDataset.prototype.setAccessToken = function(token) {
        this.token = token;
        this.gregorianCalendar = new gregorian.GregorianCalendar();
    };

    TheMovieDBDataset.prototype.setMaxPages = function(maxPages) {
        if (maxPages === null) {
            this.maxPages = DEFAULT_MAX_PAGES;
        } else {
            this.maxPages = maxPages;
        }
    };

    TheMovieDBDataset.prototype.getGenres = function() {
        var self = this;
        return new Promise(function(resolve, reject) {
            self.get("/genre/movie/list").then(function(json) {
                var resp = JSON.parse(json);
                resolve(resp.genres);
            });
        });
    };

    TheMovieDBDataset.prototype.searchPerson = function(query) {
        var self = this;
        return new Promise(function(resolve, reject) {
            self.get("/search/person?query=" + query).then(function(json) {
                var resp = JSON.parse(json);
                resolve(resp.results.map(r => ({ id: r.id, name: r.name })));
            });
        });
    };

    TheMovieDBDataset.prototype.setGenres = function(genres) {
        this.genres = genres;
    };

    TheMovieDBDataset.prototype.setPeople = function(people) {
        this.people = people;
    };

    TheMovieDBDataset.prototype.fetch = function() {
        var self = this;
        return new Promise(function(resolve, reject) {
            if (serverConfig === null) {
                self.get("/configuration").then(function(json) {
                    var resp = JSON.parse(json);
                    serverConfig = {
                        imageBaseURL: resp.images.base_url,
                        imageSize: "original",
                    };
                    self.fetchMovies(resolve, reject);
                });
            } else {
                self.fetchMovies(resolve, reject);
            }
        });
    };

    return {
        TheMovieDBDataset: TheMovieDBDataset,
    };
});
