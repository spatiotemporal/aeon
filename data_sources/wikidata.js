define(['require', '../util/http', '../calendars/gregorian'], function(require) {

    var http = require('../util/http');
    var gregorian = require('../calendars/gregorian');

    const START_TIME_PROPERTIES = ['wdt:P580', 'wdt:P523', 'wdt:P729', 'wdt:P1619', 'wdt:P1636',
                                   'wdt:P2031', 'wdt:P569'];
    const END_TIME_PROPERTIES = ['wdt:P582', 'wdt:P524', 'wdt:P576', 'wdt:P730', 'wdt:P2032', 'wdt:P2669',
                                 'wdt:P3999', 'wdt:P570'];
    const MOMENTARY_TIME_PROPERTIES = ['wdt:P585', 'wdt:P571', 'wdt:P577', 'wdt:P6949', 'wdt:P7588', 'wdt:P1191'];
    const POSITION_QUALIFIERS = ['P642', 'P108'];

    function generateRangeFilter(propertyId) {
        return function(i) {
            var filters = [];
            if (i[0] !== null) {
                filters.push("?prop_" + propertyId + " >= " + i[0]);
            }
            if (i[1] !== null) {
                filters.push("?prop_" + propertyId + " <= " + i[1]);
            }
            return "?item wdt:" + propertyId + " ?prop_" + propertyId + ". FILTER (" + filters.join("&&") + "). ";
        };
    }

    const FILTER_PATTERN_GENERATORS = {
        "instance-of": i => ("?item wdt:P31 wd:" + i + ". "),
        "instance-of-subclass-of": i => (i.map(c => ("{ ?item wdt:P31/wdt:P279* wd:" + c + ". }")).join('UNION')),
        "occupation": i => ("?item wdt:P106 wd:" + i + ". "),
        "country": i => ("?item wdt:P17 wd:" + i + ". "),
        "citizenship": i => ("?item wdt:P27 wd:" + i + ". "),
        "country-of-origin": i => ("?item wdt:P495 wd:" + i + ". "),
        "country-of-birth": i => ("?item wdt:P19 ?cb_place. ?cb_place wdt:P17 wd:" + i + ". "),
        "region-of-birth": i => ("{ ?item wdt:P19 wd:" + i + ". } UNION { ?item wdt:P19 ?rb_place. ?rb_place wdt:P131 wd:"
                                 + i + ". }"),
        "country-of-death": i => ("?item wdt:P20 ?cd_place. ?cd_place wdt:P17 wd:" + i + ". "),
        "region-of-death": i => ("{ ?item wdt:P20 wd:" + i + ". } UNION { ?item wdt:P20 ?rd_place. ?rd_place wdt:P131 wd:"
                                 + i + ". }"),
        "country-of-burial": i => ("?item wdt:P119 ?cu_place. ?cu_place wdt:P17 wd:" + i + ". "),
        "region-of-burial": i => ("{ ?item wdt:P119 wd:" + i
                                  + ". } UNION { ?item wdt:P119 ?ru_place. ?ru_place wdt:P131 wd:"
                                  + i + ". }"),
        "gender": i => ("?item wdt:P21/wdt:P279* wd:" + i + ". "),
        "manner-of-death": i => ("?item wdt:P1196/wdt:P279* wd:" + i + ". "),
        "native-language": i => ("?item wdt:P103/wdt:P279* wd:" + i + ". "),
        "used-language": i => ("?item wdt:P1412/wdt:P279* wd:" + i + ". "),
        "language-of-work-or-name": i => ("?item wdt:P407/wdt:P279* wd:" + i + ". "),
        "genre": i => ("{ ?item wdt:P136/wdt:P279* wd:" + i + ". } UNION { ?item wdt:P31/wdt:P279* wd:" + i + ". }"),
        "movement": i => ("?item wdt:P135/wdt:P279* wd:" + i + ". "),
        "religion": i => ("?item wdt:P140/wdt:P279* wd:" + i + ". "),
        "political_party": i => ("?item wdt:P102 wd:" + i + ". "),
        "musical_ensemble": i => ("?item wdt:P463 wd:" + i + ". "),
        "sport": i => ("?item wdt:P641 wd:" + i + ". "),
        "sports_team": i => ("?item wdt:P54 wd:" + i + ". "),
        "award": i => ("?item wdt:P166 wd:" + i + ". "),
        "position": i => ("?item p:P39 ?child. "
                          + "?child ps:P39 wd:"+ i[0] + ". "
                          + ((i[1] !== null)
                             ? ("?child ?qual wd:" + i[1] + ". "
                                + "VALUES ?qual {" + POSITION_QUALIFIERS.map(q => ("pq:" + q)).join(" ") + "}. ")
                             : "")
                          + "?child pq:P580 ?cstart. "
                          + "OPTIONAL { ?child pq:P582 ?cend }. "),
        "sub-event": i => ("OPTIONAL { ?item ?childprop ?child. ?child rdfs:label ?clabel. "
                           + "FILTER(LANG(?clabel) = \"en\"). "
                           + "OPTIONAL{?child schema:description ?cdescription. "
                           + "FILTER(LANG(?cdescription) = \"en\").} "
                           + "?cwikipedia_url schema:about ?child. "
                           + "?cwikipedia_url schema:isPartOf <https://en.wikipedia.org/>. "
                           + "?child ?cstartprop ?cstart. OPTIONAL { ?child wdt:P582 ?cend }. "
                           + "FILTER(?cstartprop IN (wdt:P580,wdt:P585)). "
                           + "FILTER(?childprop IN (wdt:P793,wdt:P527)). "
                           + "OPTIONAL{?child wdt:P18 ?cimage.} } "),
        "copyright-license": i => ("?item wdt:P275/wdt:P279* wd:" + i + ". "),
        "number-of-participants": generateRangeFilter("P1132"),
        "number-of-survivors": generateRangeFilter("P1561"),
        "number-of-injured": generateRangeFilter("P1339"),
        "number-of-deaths": generateRangeFilter("P1120"),
    };

    const MAX_RESULTS = 100;

    var proxyDataCache = {};

    function WikidataSet(databaseEndpoint, proxyEndpoint) {
        var self = this;

        if (typeof databaseEndpoint === "undefined") {
            this.databaseEndpoint = "https://query.wikidata.org/sparql";
        } else {
            this.databaseEndpoint = databaseEndpoint;
        }

        if (typeof proxyEndpoint === "undefined") {
            this.proxyEndpoint = "data/wikidata.json";
        } else {
            this.proxyEndpoint = proxyEndpoint;
        }

        if (!(proxyEndpoint in proxyDataCache)) {
            this.proxyDataPromise = proxyDataCache[proxyEndpoint] = new Promise(function(resolve, reject) {
                http.get(self.proxyEndpoint).then(function(json) {
                    resolve(JSON.parse(json));
                });
            });
        }
        this.proxyDataPromise = proxyDataCache[proxyEndpoint];

        this.gregorianCalendar = new gregorian.GregorianCalendar();

        this.parseDateString = function(s) {
            var r = s.match(/([-0-9]+)-([0-9]+)-([0-9]+)T([0-9]+):([0-9]+):([0-9]+)Z/);
            if (r === null || r.length < 7) {
                return null;
            }
            var n = r.slice(1).map(x => parseInt(x, 10));
            var gregorianDate = [n[0], n[1], n[2]];
            if (gregorianDate[0] <= 0) {
                gregorianDate[0]--;
            }
            return this.gregorianCalendar.dateToTimestamp(gregorianDate) + 3600 * n[3] + 60 * n[4] + n[5];
        };

        this.setSPARQLPatterns = function(patterns) {
            this.query =
                'SELECT DISTINCT ?item ?label ?description ?start ?end ?startprop ?wikipedia_url ?image '
                + '?child ?clabel ?cdescription ?cstart ?cend ?cwikipedia_url ?cimage '
                + 'WHERE '
                + '{ '
                + patterns + ' '
                + '?item rdfs:label ?label. '
                + 'FILTER(LANG(?label) = "en"). '
                + 'OPTIONAL{?item schema:description ?description. '
                + 'FILTER(LANG(?description) = "en").} '
                + '?wikipedia_url schema:about ?item . '
                + '?wikipedia_url schema:isPartOf <https://en.wikipedia.org/>. '
                + '?item ?startprop ?start. '
                + 'FILTER(?startprop IN (' + START_TIME_PROPERTIES.concat(MOMENTARY_TIME_PROPERTIES).join(',') + ')). '
                + 'OPTIONAL{?item ' + END_TIME_PROPERTIES.join('|') + '?end.} '
                + 'OPTIONAL{?item wdt:P18 ?image.} '
                + '} LIMIT ' + MAX_RESULTS;
        };
    }

    WikidataSet.prototype.getOccupations = function(id) {
        var self = this;
        return new Promise(function(resolve, reject) {
            self.proxyDataPromise.then(function(data) {
                resolve(data["occupations"]);
            });
        });
    };

    WikidataSet.prototype.getCountries = function() {
        var self = this;
        return new Promise(function(resolve, reject) {
            self.proxyDataPromise.then(function(data) {
                resolve(data["countries"]);
            });
        });
    };

    WikidataSet.prototype.getFirstLevelCountrySubdivisions = function(countryID, labelLanguages) {
        var self = this;
        return new Promise(function(resolve, reject) {
            var query = 'SELECT ?item ?itemLabel '
                + 'WHERE '
                + '{ '
                + '?item wdt:P31/wdt:P279* wd:Q10864048. '
                + '?item wdt:P17 wd:' + countryID + '. '
                + 'SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],'
                + labelLanguages.join(",") + '". } '
                + '}';
            var url = self.databaseEndpoint + "?format=json&query=" + encodeURIComponent(query);
            http.get(url).then(function(json) {
                var res = JSON.parse(json).results.bindings;
                var a = [];
                for (let r of res) {
                    a.push({
                        id: r.item.value.split("/").pop(),
                        name: r.itemLabel.value,
                    });
                }
                resolve(a);
            });
        });
    };

    WikidataSet.prototype.getSecondLevelCountrySubdivisions = function(firstLevelSubdivisionID, labelLanguages) {
        var self = this;
        return new Promise(function(resolve, reject) {
            var query = 'SELECT ?item ?itemLabel '
                + 'WHERE '
                + '{ '
                + '?item wdt:P31/wdt:P279* wd:Q13220204. '
                + '?item wdt:P131 wd:' + firstLevelSubdivisionID + '. '
                + 'SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],'
                + labelLanguages.join(",") + '". } '
                + '}';
            var url = self.databaseEndpoint + "?format=json&query=" + encodeURIComponent(query);
            http.get(url).then(function(json) {
                var res = JSON.parse(json).results.bindings;
                var a = [];
                for (let r of res) {
                    a.push({
                        id: r.item.value.split("/").pop(),
                        name: r.itemLabel.value,
                    });
                }
                resolve(a);
            });
        });
    };

    WikidataSet.prototype.getLanguages = function() {
        var self = this;
        return new Promise(function(resolve, reject) {
            self.proxyDataPromise.then(function(data) {
                resolve(data["languages"]);
            });
        });
    };

    WikidataSet.prototype.getGenres = function() {
        var self = this;
        return new Promise(function(resolve, reject) {
            self.proxyDataPromise.then(function(data) {
                resolve(data["genres"]);
            });
        });
    };

    WikidataSet.prototype.getMovements = function() {
        var self = this;
        return new Promise(function(resolve, reject) {
            self.proxyDataPromise.then(function(data) {
                resolve(data["movements"]);
            });
        });
    };

    WikidataSet.prototype.getReligions = function() {
        var self = this;
        return new Promise(function(resolve, reject) {
            self.proxyDataPromise.then(function(data) {
                resolve(data["religions"]);
            });
        });
    };

    WikidataSet.prototype.getPoliticalParties = function() {
        var self = this;
        return new Promise(function(resolve, reject) {
            self.proxyDataPromise.then(function(data) {
                resolve(data["political_parties"]);
            });
        });
    };

    WikidataSet.prototype.getMusicalEnsembles = function() {
        var self = this;
        return new Promise(function(resolve, reject) {
            self.proxyDataPromise.then(function(data) {
                resolve(data["musical_ensembles"]);
            });
        });
    };

    WikidataSet.prototype.getSports = function() {
        var self = this;
        return new Promise(function(resolve, reject) {
            self.proxyDataPromise.then(function(data) {
                resolve(data["sports"]);
            });
        });
    };

    WikidataSet.prototype.getSportTeams = function() {
        var self = this;
        return new Promise(function(resolve, reject) {
            self.proxyDataPromise.then(function(data) {
                resolve(data["sports_teams"]);
            });
        });
    };

    WikidataSet.prototype.getAwards = function() {
        var self = this;
        return new Promise(function(resolve, reject) {
            self.proxyDataPromise.then(function(data) {
                resolve(data["awards"]);
            });
        });
    };

    WikidataSet.prototype.getPositions = function() {
        var self = this;
        return new Promise(function(resolve, reject) {
            self.proxyDataPromise.then(function(data) {
                resolve(data["positions"]);
            });
        });
    };

    WikidataSet.prototype.getCopyrightLicenses = function() {
        var self = this;
        return new Promise(function(resolve, reject) {
            self.proxyDataPromise.then(function(data) {
                resolve(data["copyright_licenses"]);
            });
        });
    };

    WikidataSet.prototype.positionNeedsQualifier = function(position) {
        var self = this;
        return new Promise(function(resolve, reject) {
            var query = 'ASK '
                + '{ '
                + POSITION_QUALIFIERS.map(
                    q => ('OPTIONAL { ?someone p:P39 [ ps:P39 wd:' + position + '; pq:' + q + ' ?org_' + q + '; ]. } '))
                .join('')
                + 'FILTER ( ' + POSITION_QUALIFIERS.map(q => ('bound(?org_' + q + ')')).join('||') + '). '
                + '}';
            var url = self.databaseEndpoint + "?format=json&query=" + encodeURIComponent(query);
            http.get(url).then(function(json) {
                resolve(JSON.parse(json).boolean);
            });
        });
    };

    WikidataSet.prototype.getOrganizationsWithPosition = function(position) {
        var self = this;
        return new Promise(function(resolve, reject) {
            var query = 'SELECT DISTINCT ?item ?itemLabel '
                + 'WHERE '
                + '{ '
                + '{ '
                + '?item wdt:P31 wd:Q43229. '
                + '} UNION { '
                + '?item wdt:P31/wdt:P279 wd:Q43229. '
                + '} UNION { '
                + '?item wdt:P31/wdt:P279/wdt:P279 wd:Q43229. '
                + '} '
                + POSITION_QUALIFIERS.map(
                    q => ('{ ?someone p:P39 [ ps:P39 wd:' + position + '; pq:' + q + ' ?item; ]. }'))
                        .join('UNION')
                + '?someone rdfs:label ?label. '
                + 'FILTER(LANG(?label) = "en"). '
                + '?wikipedia_url schema:about ?someone. '
                + '?wikipedia_url schema:isPartOf <https://en.wikipedia.org/>. '
                + 'SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". } '
                + '}';
            var url = self.databaseEndpoint + "?format=json&query=" + encodeURIComponent(query);
            http.get(url).then(function(json) {
                var res = JSON.parse(json).results.bindings;
                var a = [];
                for (let r of res) {
                    a.push({
                        id: r.item.value.split("/").pop(),
                        name: r.itemLabel.value,
                    });
                }
                resolve(a);
            });
        });
    };

    WikidataSet.prototype.setFilters = function(filters) {
        var patterns = "";
        for (let [f, v] of Object.entries(filters)) {
            if (f in FILTER_PATTERN_GENERATORS) {
                patterns += FILTER_PATTERN_GENERATORS[f](v);
            }
        }
        this.setSPARQLPatterns(patterns);
    };

    WikidataSet.prototype.fetch = function() {
        var self = this;
        return new Promise(function(resolve, reject) {
            var url = self.databaseEndpoint + "?format=json&query=" + encodeURIComponent(self.query);
            http.get(url).then(function(json) {
                var a = {
                    type: 'entity',
                    dataset: self,
                    data: [],
                };
                var res = JSON.parse(json).results.bindings;
                for (let r of res) {
                    var id = r.item.value.split("/").pop();
                    var cid = ("child" in r) ? r.child.value.split("/").pop() : null;
                    var parent = a.data.find(obj => obj.id === id);
                    if (parent === undefined) {
                        var o = {
                            id: id,
                            title: r.label.value,
                            description: "description" in r ? r.description.value : null,
                            links: [],
                            start: self.parseDateString(r.start.value),
                            end: "end" in r ? self.parseDateString(r.end.value) : null,
                            image: "image" in r ? r.image.value : null,
                        };
                        if (o.end === null
                            && MOMENTARY_TIME_PROPERTIES.includes("wdt:" + r.startprop.value.split("/").pop())) {
                            o.end = o.start;
                        }
                        if ("wikipedia_url" in r) {
                            o.links.push({ resource: "Wikipedia", url: r.wikipedia_url.value });
                        }
                        if (o.start === null) {
                            continue;
                        }
                        a.data.push(o);
                        parent = o;
                    }
                    if ("cstart" in r && a.data.find(obj => obj.id === cid) === undefined) {
                        var o = {
                            id: cid,
                            parent: parent,
                            title: "clabel" in r ? r.clabel.value : "",
                            description: "cdescription" in r ? r.cdescription.value : null,
                            links: [],
                            start: self.parseDateString(r.cstart.value),
                            end: "cend" in r ? self.parseDateString(r.cend.value) : o.end,
                            image: "cimage" in r ? r.cimage.value : null,
                        };
                        if ("cwikipedia_url" in r) {
                            o.links.push({ resource: "Wikipedia", url: r.cwikipedia_url.value });
                        }
                        a.data.push(o);
                    }
                }
                resolve(a);
            });
        });
    };

    return {
        WikidataSet: WikidataSet,
    };
});
