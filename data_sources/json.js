define(['require', '../util/http'], function(require) {

    var http = require('../util/http');

    const FORMATS = {
        "aeon-entities": {
            internal_type: "entity",
            required_globals: [],
            required_fields: ['title', 'start', 'end'],
            optional_fields: ['description', 'links', 'image'],
        },
        "aeon-numerical": {
            internal_type: "num",
            required_globals: ['kind'],
            required_fields: ['time', 'value'],
            optional_fields: [],
        },
    };

    function JSONDataset() {
        var self = this;
        this.url = this.json = null;

        this.parse = function(json) {
            var res = JSON.parse(json);

            if (! "format" in res || ! res.format in FORMATS || ! "data" in res)
            {
                return null;
            }

            var a = {
                type: FORMATS[res.format].internal_type,
                dataset: self,
                data: [],
            };

            FORMATS[res.format].required_globals.forEach(function(global) {
                if (global in res) {
                    a[global] = res[global];
                } else {
                    return null;
                }
            });

            for (var i = 0; i < res.data.length; i++) {
                var o = { id: i };
                var valid = true;

                FORMATS[res.format].optional_fields.forEach(function(field) {
                    if (field in res.data[i]) {
                        o[field] = res.data[i][field];
                    } else {
                        o[field] = null;
                    }
                });

                FORMATS[res.format].required_fields.forEach(function(field) {
                    if (field in res.data[i]) {
                        o[field] = res.data[i][field];
                    } else {
                        valid = false;
                    }
                });

                if (valid) {
                    a.data.push(o);
                }
            }

            return a;
        };
    };

    JSONDataset.prototype.setJSON = function(json) {
        this.json = json;
        this.url = null;
    };

    JSONDataset.prototype.setURL = function(url) {
        this.url = url;
        this.json = null;
    };

    JSONDataset.prototype.fetch = function() {
        var self = this;
        return new Promise(function(resolve, reject) {
            if (self.json !== null) {
                resolve(self.parse(self.json));
            } else if (self.url !== null) {
                http.get(self.url).then(function(json) {
                    resolve(self.parse(json));
                });
            } else {
                resolve([]);
            }
        });
    };

    return {
        JSONDataset: JSONDataset,
    };
});
