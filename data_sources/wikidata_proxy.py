#!/usr/bin/env python3

import sys
import re
import json
import time

from urllib.parse import quote
from urllib.request import Request, urlopen
from urllib.error import URLError, HTTPError

NUMBER_OF_ATTEMPTS = 3
RETRY_HOLDOFF_TIME = 10

ENDPOINT = "https://query.wikidata.org/sparql"
MAIN_IDENTIFIER = "item"
MAIN_LABEL = "itemLabel"

def sort_alphabetically(r):
    r.sort(key=lambda x: x["name"].lower())
    return r

def sort_alphabetically_and_strip_language_suffixes(r):
    r = sort_alphabetically(r)
    for e in r:
        e["languages"] = [lang.split("-")[0] for lang in e["languages"]]
    return r

def unique_name_sort_lower_case_first(r):
    r.sort(key=lambda x: (1 if x["name"].islower() else 2, x["name"].lower(), int(x["id"][1:])))
    return [x for i, x in enumerate(r) if i == 0 or x["name"] != r[i - 1]["name"]]

QUERIES = {
    # Instance of occupation (Q12737077) or profession (Q28640).
    "occupations": {
        "sparql": """
            SELECT ?item ?itemLabel
            WHERE
            {
              {
                ?item wdt:P31 wd:Q12737077.
              } UNION {
                ?item wdt:P31 wd:Q28640.
              }
              SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
            }
        """,
        "postprocess": None,
    },
    # Instance of country (Q6256). Get list of official languages (P37) for each country.
    "countries": {
        "sparql": """
            SELECT ?item ?itemLabel ?languages
            WHERE
            {
              ?item wdt:P31 wd:Q6256.
              ?item wdt:P37 ?lang.
              ?lang wdt:P424 ?languages.
              SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
            }
        """,
        "postprocess": sort_alphabetically_and_strip_language_suffixes,
    },
    # Instance of (subclass of) language
    "languages": {
        "sparql": """
            SELECT ?item ?itemLabel
            WHERE
            {
              ?item wdt:P31/wdt:P279* wd:Q34770.
              SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
            }
        """,
        "postprocess": sort_alphabetically,
    },
    # Instance of classes in value-type constraint of Genre (P136).
    "genres": {
        "sparql": """
            SELECT ?item ?itemLabel
            WHERE
            {
                ?item wdt:P31 ?class.
                VALUES ?class {
                    wd:Q483394 wd:Q11921029 wd:Q1406161 wd:Q1792644 wd:Q17155032 wd:Q17955 wd:Q3030248 wd:Q53001749
                    wd:Q862597 wd:Q1499017 wd:Q223393 wd:Q188451 wd:Q4263830 wd:Q107356781
                }.
                SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
            }
        """,
        "postprocess": sort_alphabetically,
    },
    # Instance of classes in value-type constraint of Movement (P135).
    "movements": {
        "sparql": """
            SELECT ?item ?itemLabel
            WHERE
            {
                ?item wdt:P31 ?class.
                VALUES ?class {
                    wd:Q3533467 wd:Q1387659 wd:Q16895642 wd:Q2738074 wd:Q2198855 wd:Q23834194 wd:Q49773 wd:Q2915955
                    wd:Q28820001 wd:Q968159
                }.
                SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
            }
        """,
        "postprocess": sort_alphabetically,
    },
    # Instance of (subclass of) religion
    "religions": {
        "sparql": """
            SELECT ?item ?itemLabel
            WHERE
            {
                ?item wdt:P31/wdt:P279* wd:Q9174.
                SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
            }
        """,
        "postprocess": sort_alphabetically,
    },
    # Instance of (subclass of) political party
    "political_parties": {
        "sparql": """
            SELECT ?item ?itemLabel
            WHERE
            {
                ?item wdt:P31 wd:Q7278.
                SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
            }
        """,
        "postprocess": sort_alphabetically,
    },
    # Instance of (subclass of) musical ensemble
    "musical_ensembles": {
        "sparql": """
            SELECT ?item ?itemLabel
            WHERE
            {
                ?item wdt:P31/wdt:P279* wd:Q2088357.
                SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
            }
        """,
        "postprocess": sort_alphabetically,
    },
    # Instance of (subclass of) sport
    "sports": {
        "sparql": """
            SELECT ?item ?itemLabel
            WHERE
            {
              ?item wdt:P31/wdt:P279* wd:Q349.
              SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
            }
        """,
        "postprocess": sort_alphabetically,
    },
    # Instance of (subclass of) sports team
    "sports_teams": {
        "sparql": """
            SELECT ?item ?itemLabel
            WHERE
            {
                ?item wdt:P31/wdt:P279* wd:Q12973014.
                SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
            }
        """,
        "postprocess": sort_alphabetically,
    },
    # Instance of direct subclass of award, plus important awards further down in the tree
    "awards": {
        "sparql": """
            SELECT DISTINCT ?item ?itemLabel
            WHERE
            {
                {
                    ?item wdt:P31/wdt:P279? wd:Q618779.
                } UNION {
                    ?item wdt:P31/wdt:P279 wd:Q11448906.
                } UNION {
                    ?item wdt:P31/wdt:P279* wd:Q19020.
                } UNION {
                    ?item wdt:P31/wdt:P279* wd:Q28444913.
                } UNION {
                    ?item wdt:P31/wdt:P279* wd:Q223313.
                }
                SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
            }
        """,
        "postprocess": sort_alphabetically,
    },
    "positions": {
        "sparql": """
            SELECT DISTINCT ?item ?itemLabel
            WHERE
            {
                {
                    ?item wdt:P31 ?class.
                } UNION {
                    ?item wdt:P31/wdt:P279 ?class.
                }
                VALUES ?class { wd:Q4164871 wd:Q21451536 wd:Q355567 wd:Q3687335 wd:Q7810129 wd:Q81752537 }.
                SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
            }
        """,
        "postprocess": unique_name_sort_lower_case_first,
    },
    "copyright_licenses": {
        "sparql": """
            SELECT ?item ?itemLabel
            WHERE
            {
                ?item wdt:P31/wdt:P279* wd:Q77205602.
                SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
            }
        """,
        "postprocess": sort_alphabetically,
    },
}

def collapse(r, key_field):
    result = {}
    for entry in r:
        key = entry[key_field]["value"].split("/")[-1]
        del entry[key_field]
        if key in result.keys():
            for k, v in entry.items():
                if not v["value"] in result[key][k]:
                    result[key][k].append(v["value"])
        else:
            r = {}
            for k, v in entry.items():
                r[k] = [v["value"]]
            result[key] = r
    return result

def remove_entries_without_label(r, label_field):
    result = []
    for k, v in r.items():
        if k not in v[label_field]:
            v["id"] = k
            v["name"] = v[label_field][0]
            del v[label_field]
            result.append(v)
    return result

try:
    output_file_path = sys.argv[1]
except IndexError:
    output_file_path = None

result = {}

for name, query in QUERIES.items():
    url = "%s?format=json&query=%s" % (ENDPOINT, quote(re.sub(" +", " ", query["sparql"])))

    for attempt in range(NUMBER_OF_ATTEMPTS):
        if attempt > 0:
            time.sleep(RETRY_HOLDOFF_TIME)
        try:
            with urlopen(url) as response:
                r = json.loads(response.read())["results"]["bindings"]
                break
        except URLError:
            pass
    else:
        sys.stderr.write("Gave up downloading resource '%s' after %d attempts\n" % (name, NUMBER_OF_ATTEMPTS))
        sys.exit(1)

    r = remove_entries_without_label(collapse(r, MAIN_IDENTIFIER), MAIN_LABEL)
    if query["postprocess"] is not None:
        result[name] = query["postprocess"](r)
    else:
        result[name] = r

json_output = json.dumps(result, indent=4)

if output_file_path:
    with open(output_file_path, "w", encoding = "utf-8") as ofile:
        ofile.write(json_output)
else:
    print(json_output)
