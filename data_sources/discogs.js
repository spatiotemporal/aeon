define(['require', '../util/http', '../calendars/gregorian'], function(require) {

    var http = require('../util/http');
    var gregorian = require('../calendars/gregorian');

    const ENDPOINT = "https://api.discogs.com";

    function DiscogsDataset() {
        var self = this;
        this.authKey = null;
        this.authSecret = null;
        this.artist = null;
        this.gregorianCalendar = new gregorian.GregorianCalendar();

        this.get = function(request, usePages, useAuth) {
            var pageRequest = usePages ? "&per_page=100" : "";
            var authRequest = usePages ? "&key=" + self.authKey + "&secret=" + self.authSecret : "";
            return http.get(ENDPOINT + request + pageRequest + authRequest);
        };

    };

    DiscogsDataset.prototype.setAuthKey = function(key, secret) {
        this.authKey = key;
        this.authSecret = secret;
    };

    DiscogsDataset.prototype.searchArtists = function(string) {
        var self = this;
        return new Promise(function(resolve, reject) {
            self.get("/database/search?q=" + string + "&type=artist", true, true).then(function(json) {
                var resp = JSON.parse(json);
                resolve(resp.results.map(r => ({ id: r.id, name: r.title })));
            });
        });
    };

    DiscogsDataset.prototype.setArtist = function(artist) {
        this.artist = artist;
    };

    DiscogsDataset.prototype.getReleaseDetails = function(release, isMaster) {
        var self = this;
        return new Promise(function(resolve, reject) {
            self.get((isMaster ? "/masters/" : "/releases/") + release, false, false).then(function(json) {
                var resp = JSON.parse(json);
                var t = self.gregorianCalendar.dateToTimestamp([resp.year, 1, 1]);
                var links = [], fragments = [];
                if ("uri" in resp) {
                    links.push({resource: "Discogs entry", url: resp.uri});
                }
                if ("artists" in resp) {
                    fragments.push({ name: "Artists", fragment: resp.artists.map(a => a.name).join(", ") });
                }
                if ("genres" in resp) {
                    fragments.push({ name: "Genres", fragment: resp.genres.join(", ") });
                }
                if ("styles" in resp) {
                    fragments.push({ name: "Styles", fragment: resp.styles.join(", ") });
                }
                resolve({
                    links: links,
                    info_fragments: fragments,
                });
            });
        });
    };

    DiscogsDataset.prototype.fetch = function() {
        var self = this;
        return new Promise(function(resolve, reject) {
            self.get("/artists/" + self.artist + "/releases?sort=year&sort_order=asc", true, true).then(function(json) {
                var resp = JSON.parse(json);
                var a = {
                    type: 'entity',
                    dataset: self,
                    data: resp.releases.map(function(r) {
                        var t = self.gregorianCalendar.dateToTimestamp([r.year, 1, 1]);
                        return {
                            id: r.id,
                            title: r.title,
                            start: t,
                            end: t,
                            image: r.thumb,
                            more_info_cb: function() { return self.getReleaseDetails(r.id, r.type == "master"); },
                        };
                    }),
                };
                resolve(a);
            });
        });
    };

    return {
        DiscogsDataset: DiscogsDataset,
    };
});
