define(['require', '../util/http', '../calendars/gregorian'], function(require) {

    var http = require('../util/http');
    var gregorian = require('../calendars/gregorian');

    const ENDPOINT = "https://apps.who.int/gho/athena/api/";
    const HIDDEN_DIMENSIONS = [
        "GHO",
        "PUBLISHSTATE",
        "YEAR", "MONTH", "WEEK", "DAY",
        "DATASOURCE",
        "WORLDBANKINCOMEGROUP",
    ];
    const LOCATION_DIMENSIONS = [
        "COUNTRY",
        "REGION",
        "UNREGION",
        "UNSDGREGION",
        "WORLDBANKREGION",
        "WHOINCOMEREGION",
        "GBDREGION",
    ];
    const HIDDEN_CATEGORIES = ["0",
                               "Essential health technologies",
                               "Global Observatory for eHealth (GOe)",
                               "Health Equity Monitor",
                               "ICD",
                               "International Health Regulations (2005) monitoring framework",
                               "Malaria",
                               "Noncommunicable diseases CCS",
                               "Nutrition",
                               "Oral health",
                               "Public health and environment",
                               "Substance use and mental health",
                               "Urban health",
                               "(uncategorized)",
                              ];
    const HIDDEN_INDICATORS = ["GASPRP", "AMRGLASS_COUNTRIES01", "AMRGLASS_COUNTRIES02", "AMRGLASS_COUNTRIES03",
                               "WHS9_CS",
                               "FINPROTECTION_P1_190_POP", "FINPROTECTION_P1_310_POP",
                               "HIV_0000000002", "HIV_0000000003", "HIV_0000000004", "HIV_0000000005", "HIV_0000000010",
                               "HIV_0000000012", "HIV_0000000013", "HIV_0000000014", "HIV_0000000016", "HIV_0000000023",
                               "HIV_0000000029",
                               "TB_c_tsr", "HRH_01", "HRH_02", "HRH_03", "HRH_04",
                               "SDG_SH_STA_SCIDEN",
                               "TOBACCO_0000000192",
                              ];
    const INDICATORS_WITH_BAD_XML = ["NUTOVERWEIGHTNUM", "NUTSTUNTINGNUM", "NUTUNDERWEIGHTNUM", "LBW_NUMBER"];
    const CATEGORY_ALIASES = {
        "Neglected Tropical Diseases": "Neglected tropical diseases",
        "Negelected tropical diseases": "Neglected tropical diseases",
        "UHC": "Universal Health Coverage",
        "Noncommunicable diseases and mental health": "Noncommunicable diseases",
        "TOBACCO": "Tobacco",
    };
    const DATATYPE_MAPPINGS = [
        {
            matchDataType: "Rate",
            kind: "count",
        },
        {
            matchTitleRegexp: "\\((in )?thousands\\)$",
            kind: "count",
            suggestedUnit: "thousands",
            scaleFactor: 1000,
        },
        {
            matchTitleRegexp: "\\((in )?millions\\)$",
            kind: "count",
            suggestedUnit: "millions",
            scaleFactor: 1000000,
        },
        {
            fixedIndicators: ["GASPRSAZM", "GASPRSCFM", "GASPRSCIP", "GASPRSCRO", "GASPRSESC",
                              "SDGOOP", "CHILDVIOL"],
            matchDataType: "Percent",
            matchTitleRegexp: "\\(%\\)$",
            kind: "ratio",
            suggestedUnit: "percent",
            scaleFactor: 0.01,
        },
        {
            matchDataType: "Statistic",
            matchUoM: "Years",
            matchTitleRegexp: "\\(years?\\)$",
            kind: "time",
            suggestedUnit: "years",
            scaleFactor: 31556926,
        },
        {
            fixedIndicators: ["BP_05", "BP_06"],
            kind: "pressure",
            suggestedUnit: "mm Hg",
            scaleFactor: 133.322368,
        },
        {
            fixedIndicators: ["CHOL_03", "CHOL_04", "NCD_GLUC_01", "NCD_GLUC_02"],
            kind: "concentration",
            suggestedUnit: "mmol/L",
            scaleFactor: 1,
        },
        {
            fixedIndicators: ["NCD_BMI_MEANC"],
            kind: "body_mass_index",
            scaleFactor: 1,
        },
    ];
    const DATATYPE_MAPPING_FALLBACK = {
        kind: "count",
    };

    function WHODataset() {
        var self = this;
        this.indicator = null;   // Currently selected indicator
        this.indicators = null;  // All available indicators grouped by category
        this.dimensions = null;  // Array of dimensions for currently selected indicator
        this.gregorianCalendar = new gregorian.GregorianCalendar();

        this.getIndicatorName = function(indicator) {
            for (let cat of Object.values(self.indicators)) {
                var ind = cat.find(i => (i.id == indicator));
                if (ind) {
                    return ind.name;
                }
            }
            return null;
        };

        this.populateData = function(a, resp, mapping) {
            if (!self.indicators) {
                // Download indicator tree and schedule this function to be recalled after that.
                self.getIndicators().then(function() { self.populateData(a, resp, mapping) });
                return;
            }

            for (let m of DATATYPE_MAPPINGS) {
                if ("fixedIndicators" in m && m.fixedIndicators.indexOf(self.indicator) != -1) {
                    mapping = m;
                    break;
                }
            }

            if (!mapping) {
                for (let m of DATATYPE_MAPPINGS) {
                    if ("matchTitleRegexp" in m
                        && (new RegExp(m.matchTitleRegexp)).test(self.getIndicatorName(self.indicator))) {
                        mapping = m;
                        break;
                    }
                }
            }

            if (!mapping) {
                console.warn("Unknown unit");
                mapping = DATATYPE_MAPPING_FALLBACK;
            }

            var scaleFactor = 1;
            a.kind = mapping.kind;
            if ("suggestedUnit" in mapping) {
                a.suggestedUnit = mapping.suggestedUnit;
            }
            if ("scaleFactor" in mapping) {
                scaleFactor = mapping.scaleFactor;
            }
            a.data = resp.fact.filter(function(fact) {
                return fact.Dim.find(d => d.category == "YEAR") !== undefined
                    && "value" in fact && "numeric" in fact.value;
            }).map(function(fact) {
                return {
                    "time": self.gregorianCalendar.dateToTimestamp(
                        [parseInt(fact.Dim.find(d => d.category == "YEAR").code, 10), 1, 1]),
                    "value": scaleFactor * fact.value.numeric,
                };
            }).sort((a, b) => a.time - b.time);
        };
    };

    WHODataset.prototype.getIndicators = function() {
        var self = this;

        return new Promise(function(resolve, reject) {
            if (self.indicators) {
                resolve(self.indicators);
                return;
            }
            var url = ENDPOINT + "GHO?format=json"
            http.get(url).then(function(json) {
                var resp = JSON.parse(json);
                self.indicators = {};
                for (let ind of resp.dimension[0].code) {
                    if (HIDDEN_INDICATORS.includes(ind.label)) {
                        continue;
                    }
                    if (!(/[A-Z]/.test(ind.label))) {
                        continue;
                    }
                    var category;
                    try {
                        category = ind.attr.find(a => a.category == "CATEGORY").value;
                    } catch (error) {
                        category = "(uncategorized)";
                    }
                    if (HIDDEN_CATEGORIES.includes(category) || category.startsWith("RSUD:")) {
                        continue;
                    }
                    if (category in CATEGORY_ALIASES) {
                        category = CATEGORY_ALIASES[category];
                    }
                    if (!(category in self.indicators)) {
                        self.indicators[category] = [];
                    }
                    self.indicators[category].push({
                        "id": ind.label,
                        "name": ind.display,
                    });
                }
                resolve(self.indicators);
            });
        });
    };

    WHODataset.prototype.getDimensions = function(indicatorID) {
        return new Promise(function(resolve, reject) {
            var url = ENDPOINT + "GHO/" + indicatorID + "?format=json";
            http.get(url).then(function(json) {
                var resp = JSON.parse(json);
                var dimensions = [];
                var usedLoc = null;
                for (let loc of LOCATION_DIMENSIONS) {
                    if (resp.dimension.find(d => (d.label == loc))) {
                        usedLoc = loc;
                        break;
                    }
                }
                for (let dim of resp.dimension) {
                    if (!HIDDEN_DIMENSIONS.includes(dim.label)
                        && (!LOCATION_DIMENSIONS.includes(dim.label) || dim.label == usedLoc)) {
                        dimensions.push({
                            "id": dim.label,
                            "name": dim.display,
                            "values": dim.code.map(code => ({
                                "id": code.label,
                                "name": code.display,
                            })),
                        });
                    }
                };
                resolve(dimensions);
            });
        });
    };

    WHODataset.prototype.setDimensions = function(dimensions) {
        if (! "indicator" in dimensions) {
            console.error("Dimension 'indicator' must always be present");
            return;
        }
        this.indicator = dimensions.indicator;
        this.dimensions = dimensions;
        delete this.dimensions.indicator;
    };

    WHODataset.prototype.fetch = function() {
        var self = this;
        return new Promise(function(resolve, reject) {
            var url = ENDPOINT + "GHO/" + self.indicator
                + "?format=json&filter="
                + Object.keys(self.dimensions).map(key => (key + ":" + self.dimensions[key])).join(";");
            http.get(url).then(function(json) {
                var a = {
                    type: 'num',
                    dataset: self,
                };
                var resp;
                try {
                    resp = JSON.parse(json);
                } catch(e) {
                    a.data = [];
                    resolve(a);
                    return;
                }
                var defXML = null;
                var GHO = resp.dimension.find(d => d.label == "GHO");
                if (GHO) {
                    defXML = resp.dimension.find(d => d.label == "GHO").code[0].attr
                        .find(a => a.category == "DEFINITION_XML");
                }
                if (defXML && "value" in defXML && defXML.value.startsWith("http")
                    && !INDICATORS_WITH_BAD_XML.includes(self.indicator)) {
                    http.getXML(defXML.value).then(function(xml) {
                        var DataType = xml.getElementsByTagName("Indicators")[0]
                            .getElementsByTagName("Indicator")[0]
                            .getElementsByTagName("DataType")[0].innerHTML;
                        var UnitOfMeasure = xml.getElementsByTagName("Indicators")[0]
                            .getElementsByTagName("Indicator")[0]
                            .getElementsByTagName("UnitOfMeasure")[0].innerHTML;
                        a.description = xml.getElementsByTagName("Indicators")[0]
                            .getElementsByTagName("Indicator")[0]
                            .getElementsByTagName("Definition")[0].innerHTML;
                        a.links = Array.from(xml.getElementsByTagName("Indicators")[0]
                                             .getElementsByTagName("Indicator")[0]
                                             .getElementsByTagName("Links")).map(function(elem) {
                                                 return {
                                                     title: elem.innerHTML,
                                                     url: elem.getAttribute("url"),
                                                 };
                                             });
                        var mapping = DATATYPE_MAPPINGS.find(function(m) {
                            if ("matchUoM" in m) {
                                return m.matchDataType == DataType && m.matchUoM == UnitOfMeasure;
                            } else {
                                return m.matchDataType == DataType;
                            }
                        });
                        self.populateData(a, resp, mapping);
                        resolve(a);
                    }).catch(function() {
                        console.warn("WHO: Broken XML link");
                        self.populateData(a, resp, null);
                        resolve(a);
                    });
                } else {
                    self.populateData(a, resp, null);
                    resolve(a);
                }
            });
        });
    };

    return {
        WHODataset: WHODataset,
    };
});
