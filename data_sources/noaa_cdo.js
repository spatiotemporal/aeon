define(['require', '../util/http', '../calendars/gregorian'], function(require) {

    var http = require('../util/http');
    var gregorian = require('../calendars/gregorian');

    const BASE_URL = "https://www.ncdc.noaa.gov/cdo-web/api/v2/";
    const TOKEN = "kPpYNCCEZRaeFkBOOAHNcTRszHEwrQHI";

    const HIDDEN_DATA_TYPES = ["DYNT", "DYSD", "DYSN", "DYXP", "DYXT", "HDSD", "CDSD"];

    const DATA_TYPES = [
        {
            matchRegexp: "^(DP[0-9]{2}|DSND|DSNW|DT[0-9]{2}|DX[0-9]{2})$",
            kind: "count",
            scaleFactor: 1,
        },
        {
            matchRegexp: "^(EMNT|EMXT|TAVG|TMAX|TMIN|M[NX]PN)$",
            kind: "temperature",
            scaleFactor: 1,
        },
        {
            matchRegexp: "^[HLM][NX][0-9]{2}$",
            kind: "temperature",
            scaleFactor: 1,
            nameTransform: function(id, name) {
                return name.substr(0, name.lastIndexOf(" (degrees F)"))
                    + " (soil sample " + id[3] + ")";
            },
        },
        {
            matchRegexp: "^(CLDD|HTDD)$",
            kind: "degree_days",
            suggestedUnit: "Celcius degree days",
            scaleFactor: 24 * 3600, // degree days => Kelvin seconds
        },
        {
            matchRegexp: "^(EMSD|EVAP|PRCP|SNOW)$",
            kind: "length",
            suggestedUnit: "millimeters",
            scaleFactor: 1/1000, // mm => m
        },
        {
            matchRegexp: "^WDMV$",
            kind: "length",
            suggestedUnit: "kilometers",
            scaleFactor: 1000, // km => m
        },
        {
            matchRegexp: "^(EMSN|EMXP)$",
            kind: "volumetric_flux",
            suggestedUnit: "mm/day",
            scaleFactor: 1 / (1000 * 24 * 3600), // mm per day => m/s
        },
        {
            matchRegexp: "^PSUN$",
            kind: "ratio",
            suggestedUnit: "percent",
            scaleFactor: 1/100, // percents => pure ratio
        },
        {
            matchRegexp: "^TSUN$",
            kind: "time",
            suggestedUnit: "hours",
            scaleFactor: 1/60, // hours => seconds
        },
        {
            matchRegexp: "^(AWND|WSF1|WSF2|WSF5|WSFG|WSFM)$",
            kind: "speed",
            scaleFactor: 1,
        },
        {
            matchRegexp: "^(WDF1|WDF2|WDF5|WDFG|WDFM)$",
            kind: "angle",
            suggestedUnit: "degrees",
            scaleFactor: Math.PI / 180, // degrees => radians
        },
    ];

    function NOAACDODataset() {
        var self = this;
        this.rawCities = [];
        this.gregorianCalendar = new gregorian.GregorianCalendar();

        this.get = function(url) {
            return http.get(url, { "token": TOKEN });
        };

        this.parseDateString = function(s) {
            var r = s.match(/([-0-9]+)-([0-9]+)-([0-9]+)T([0-9]+):([0-9]+):([0-9]+)/);
            if (r === null || r.length < 7) {
                return null;
            }
            var n = r.slice(1).map(x => parseInt(x, 10));
            var gregorianDate = [n[0], n[1], n[2]];
            if (gregorianDate[0] <= 0) {
                gregorianDate[0]--;
            }
            return this.gregorianCalendar.dateToTimestamp(gregorianDate) + 3600 * n[3] + 60 * n[4] + n[5];
        };

        this.collectDataPerOffset = function(url, doneCallback, json = null, data = null) {
            var offset = 0, collectedData = [];
            if (json !== null) {
                var resp = JSON.parse(json);
                if ("results" in resp) {
                    collectedData = data.concat(resp.results);
                }
                if (!("metadata" in resp)
                    || (resp.metadata.resultset.offset + resp.metadata.resultset.limit - 1
                        >= resp.metadata.resultset.count)) {
                    doneCallback(collectedData);
                    return;
                }
                offset = resp.metadata.resultset.offset + 1000;
            }
            self.get(url + "&offset=" + offset)
                .then(function(json) { self.collectDataPerOffset(url, doneCallback, json, collectedData) });
        };

        this.collectDataPerYearIncrement = function(url, startYear, endYear, incrementYear, doneCallback,
                                                    json = null, data = []) {
            if (json !== null) {
                var resp = JSON.parse(json);
                if ("results" in resp) {
                    data = data.concat(resp.results);
                }
                if (startYear > endYear) {
                    doneCallback(data);
                    return;
                }
            }
            self.get(url + "&startdate=" + startYear + "-01-01&enddate=" + Math.min(startYear + incrementYear - 1, endYear) + "-12-31")
                .then(function(json) {
                    self.collectDataPerYearIncrement(url, startYear + incrementYear, endYear,
                                                     incrementYear, doneCallback, json, data)
                });
        };

        this.getTypeInfo = function(datatype) {
            for (let type of DATA_TYPES) {
                if (new RegExp(type.matchRegexp).test(datatype)) {
                    return type;
                }
            }
            return null;
        }

    };

    NOAACDODataset.prototype.getLocationCategories = function() {
        var self = this;
        return new Promise(function(resolve, reject) {
            self.get(BASE_URL + "locationcategories").then(function(json) {
                var resp = JSON.parse(json);
                resolve(resp.results);
            });
        });
    };

    NOAACDODataset.prototype.getLocations = function(locationCategoryID) {
        var self = this;
        return new Promise(function(resolve, reject) {
            self.collectDataPerOffset(BASE_URL + "locations?locationcategoryid=" + locationCategoryID
                                      + "&limit=1000",
                                      function(results) {
                                          resolve(results.map(function(result) {
                                              return { "id": result.id, "name": result.name };
                                          }));
                                      });
        });
    };

    NOAACDODataset.prototype.getStations = function(locationID, datasetIDs = null) {
        var self = this;
        return new Promise(function(resolve, reject) {
            self.collectDataPerOffset(BASE_URL + "stations?locationid=" + locationID
                                      + ((datasetIDs === null) ? "" : "&datasetid=" + datasetIDs.join("&datasetid="))
                                      + "&limit=1000",
                                      function(results) {
                                          resolve(results.map(function(result) {
                                              var i = result.name.lastIndexOf(",");
                                              return {
                                                  "id": result.id,
                                                  "name": (i < 0) ? result.name : (result.name.substring(0, i)),
                                                  "mindate": result.mindate, "maxdate": result.maxdate };
                                          }));
                                      });
        });
    };

    NOAACDODataset.prototype.getDatasets = function(stationID, datatypeID = null) {
        var self = this;
        return new Promise(function(resolve, reject) {
            self.get(BASE_URL + "datasets?stationid=" + stationID
                     + ((datatypeID === null) ? "" : "&datatypeid=" + datatypeID)
                     + "&limit=1000").then(function(json) {
                         var resp = JSON.parse(json);
                         if ("results" in resp) {
                             resolve(resp.results.map(function(result) {
                                 return { "id": result.id, "name": result.name };
                             }));
                         } else {
                             console.error("No datasets returned");
                             resolve([{ "id": null, name: "" }]);
                         }
                     });
        });
    };

    NOAACDODataset.prototype.getDataTypes = function(stationID, datasetIDs = null) {
        var self = this;
        return new Promise(function(resolve, reject) {
            self.get(BASE_URL + "datatypes?stationid=" + stationID
                     + ((datasetIDs === null) ? "" : "&datasetid=" + datasetIDs.join("&datasetid="))
                     + "&limit=1000")
                .then(function(json) {
                    var resp = JSON.parse(json);
                    if ("results" in resp) {
                        resolve(resp.results.filter(function(result) {
                                    return !(HIDDEN_DATA_TYPES.includes(result.id));
                                }).map(function(result) {
                                    var typeInfo = self.getTypeInfo(result.id);
                                    var name = result.name;
                                    if ("nameTransform" in typeInfo) {
                                        name = typeInfo.nameTransform(result.id, result.name);
                                    }
                                    return {
                                        "id": result.id,
                                        "name": name,
                                        "mindate": result.mindate, "maxdate": result.maxdate,
                                    };
                                }));
                    } else {
                        resolve([]);
                    }
                });
        });
    };

    NOAACDODataset.prototype.setDimensions = function(stationID, datasetID, datatypeID, startYear, endYear) {
        this.dimensions = {
            station: stationID,
            dataset: datasetID,
            datatype: datatypeID,
            startYear: parseInt(startYear, 10),
            endYear: parseInt(endYear, 10),
        };
    };


    NOAACDODataset.prototype.fetch = function() {
        var self = this;
        return new Promise(function(resolve, reject) {
            var yearIncrement = (["GSOM", "GSOY"].includes(self.dimensions.dataset)) ? 10 : 1;
            var typeInfo = self.getTypeInfo(self.dimensions.datatype);
            if (typeInfo === null) {
                reject();
                return;
            }
            self.collectDataPerYearIncrement(BASE_URL + "data?stationid=" + self.dimensions.station
                                             + "&datasetid=" + self.dimensions.dataset
                                             + "&datatypeid=" + self.dimensions.datatype
                                             + "&units=metric"
                                             + "&limit=1000&includemetadata=false",
                                             self.dimensions.startYear, self.dimensions.endYear, yearIncrement,
                                             function(results) {
                                                 var a = {
                                                     type: "num",
                                                     dataset: self,
                                                     kind: typeInfo.kind,
                                                 };
                                                 if ("suggestedUnit" in typeInfo) {
                                                     a.suggestedUnit = typeInfo.suggestedUnit;
                                                 }
                                                 a.data = results.map(function(r) {
                                                     return {
                                                         "time": self.parseDateString(r.date),
                                                         "value": typeInfo.scaleFactor * r.value,
                                                     };
                                                 });
                                                 resolve(a);
                                             });
        });
    };

    return {
        NOAACDODataset: NOAACDODataset,
    };
});
