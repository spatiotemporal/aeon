define(['require', '../util/http', '../util/csv', '../calendars/gregorian'], function(require) {

    var http = require('../util/http');
    var CSV = require('../util/csv');
    var gregorian = require('../calendars/gregorian');

    const mmpd = 1e-3 / (3600 * 24); // Millimeters per day to SI base unit

    const DESCRIPTION_GISTEMP = 'Estimate of global surface temperature relative to a reference year'
          + ' (since <a href="https://data.giss.nasa.gov/gistemp/faq/abs_temp.html">'
          + 'absolute global surface temperature is difficult to define and measure</a>).';
    const DESCRIPTION_PRECIP_CRU = 'Mean land surface precipitation.';
    const REFERENCES_GISTEMP = '<p>GISTEMP Team, 2021: <a href="https://data.giss.nasa.gov/gistemp/">'
          + 'GISS Surface Temperature Analysis (GISTEMP), version 4</a>. NASA Goddard Institute for Space Studies.</p>'
          + '<p>Lenssen, N., G. Schmidt, J. Hansen, M. Menne, A. Persin, R. Ruedy, and D. Zyss, 2019:'
          + ' <a href="https://pubs.giss.nasa.gov/abs/le05800h.html">Improvements in the GISTEMP uncertainty model.</a>'
          + ' <em>J. Geophys. Res. Atmos.</em>, <strong>124</strong>, no. 12, 6307-6326,'
          + ' doi:10.1029/2018JD029522.</p>';
    const REFERENCES_PRECIP_CRU = '<p><a href="https://data.giss.nasa.gov/precip_cru/">'
          + 'Observed Land Surface Precipitation Data: 1901-2000 (CRU TS 2.0)</a></p>'
          + '<p>Mitchell, T.D., T.R. Carter, P.D. Jones, M. Hulme, and M. New 2003.'
          + ' A comprehensive set of high-resolution grids of monthly climate for Europe and the globe:'
          + ' The observed record (1901-2000) and 16 scenarios (2001-2100). <em>J. Climate</em>, submitted.</p>';

    const DATA_FILES = [
        {
            id: "glb",
            url: "https://data.giss.nasa.gov/gistemp/tabledata_v4/GLB.Ts+dSST.csv",
            format: "csv",
            headerRow: 1,
        },
        {
            id: "zon",
            url: "https://data.giss.nasa.gov/gistemp/tabledata_v4/ZonAnn.Ts+dSST.csv",
            format: "csv",
            headerRow: 0,
        },
        {
            id: "nh",
            url: "https://data.giss.nasa.gov/gistemp/tabledata_v4/NH.Ts+dSST.csv",
            format: "csv",
            headerRow: 1,
        },
        {
            id: "sh",
            url: "https://data.giss.nasa.gov/gistemp/tabledata_v4/SH.Ts+dSST.csv",
            format: "csv",
            headerRow: 1,
        },
        {
            id: "pa",
            url: "https://data.giss.nasa.gov/precip_cru/graphs/Fig_A.txt",
            format: "txt",
            dataRow: 5,
        },
        {
            id: "pb",
            url: "https://data.giss.nasa.gov/precip_cru/graphs/Fig_B.txt",
            format: "txt",
            dataRow: 6,
        },
    ];

    const SERIES = [
        {
            id: "glob",
            name: "Global annual mean temperature",
            description: DESCRIPTION_GISTEMP,
            references: REFERENCES_GISTEMP,
            file: "zon",
            columns: [["Glob", 7]],
            scaleValue: 1,
            kind: "temperature_difference",
        },
        {
            id: "nhem",
            name: "Northern hemisphere annual mean temperature",
            description: DESCRIPTION_GISTEMP,
            references: REFERENCES_GISTEMP,
            file: "zon",
            columns: [["NHem", 7]],
            scaleValue: 1,
            kind: "temperature_difference",
        },
        {
            id: "shem",
            name: "Southern hemisphere annual mean temperature",
            description: DESCRIPTION_GISTEMP,
            references: REFERENCES_GISTEMP,
            file: "zon",
            columns: [["SHem", 7]],
            scaleValue: 1,
            kind: "temperature_difference",
        },
        {
            id: "mon",
            name: "Global monthly mean temperature",
            description: DESCRIPTION_GISTEMP,
            references: REFERENCES_GISTEMP,
            file: "glb",
            columns: [["Jan",  1], ["Feb",  2], ["Mar",  3], ["Apr",  4],
                        ["May",  5], ["Jun",  6], ["Jul",  7], ["Aug",  8],
                        ["Sep",  9], ["Oct", 10], ["Nov", 11], ["Dec", 12]],
            scaleValue: 1,
            kind: "temperature_difference",
        },
        {
            id: "nmon",
            name: "Northern hemisphere monthly mean temperature",
            description: DESCRIPTION_GISTEMP,
            references: REFERENCES_GISTEMP,
            file: "nh",
            columns: [["Jan",  1], ["Feb",  2], ["Mar",  3], ["Apr",  4],
                        ["May",  5], ["Jun",  6], ["Jul",  7], ["Aug",  8],
                        ["Sep",  9], ["Oct", 10], ["Nov", 11], ["Dec", 12]],
            scaleValue: 1,
            kind: "temperature_difference",
        },
        {
            id: "smon",
            name: "Southern hemisphere monthly mean temperature",
            description: DESCRIPTION_GISTEMP,
            references: REFERENCES_GISTEMP,
            file: "sh",
            columns: [["Jan",  1], ["Feb",  2], ["Mar",  3], ["Apr",  4],
                        ["May",  5], ["Jun",  6], ["Jul",  7], ["Aug",  8],
                        ["Sep",  9], ["Oct", 10], ["Nov", 11], ["Dec", 12]],
            scaleValue: 1,
            kind: "temperature_difference",
        },
        {
            id: "lat0",
            name: "Latitudes 90&deg; N - 64&deg; N annual mean temperature",
            description: DESCRIPTION_GISTEMP,
            references: REFERENCES_GISTEMP,
            file: "zon",
            columns: [["64N-90N", 7]],
            scaleValue: 1,
            kind: "temperature_difference",
        },
        {
            id: "lat1",
            name: "Latitudes 64&deg; N - 44&deg; N annual mean temperature",
            description: DESCRIPTION_GISTEMP,
            references: REFERENCES_GISTEMP,
            file: "zon",
            columns: [["44N-64N", 7]],
            scaleValue: 1,
            kind: "temperature_difference",
        },
        {
            id: "lat2",
            name: "Latitudes 44&deg; N - 24&deg; N annual mean temperature",
            description: DESCRIPTION_GISTEMP,
            references: REFERENCES_GISTEMP,
            file: "zon",
            columns: [["24N-44N", 7]],
            scaleValue: 1,
            kind: "temperature_difference",
        },
        {
            id: "lat3",
            name: "Latitudes 24&deg; N - 0&deg; annual mean temperature",
            description: DESCRIPTION_GISTEMP,
            references: REFERENCES_GISTEMP,
            file: "zon",
            columns: [["EQU-24N", 7]],
            scaleValue: 1,
            kind: "temperature_difference",
        },
        {
            id: "lat4",
            name: "Latitudes 0&deg; - 24&deg; S annual mean temperature",
            description: DESCRIPTION_GISTEMP,
            references: REFERENCES_GISTEMP,
            file: "zon",
            columns: [["24S-EQU", 7]],
            scaleValue: 1,
            kind: "temperature_difference",
        },
        {
            id: "lat5",
            name: "Latitudes 24&deg; S - 44&deg; S annual mean temperature",
            description: DESCRIPTION_GISTEMP,
            references: REFERENCES_GISTEMP,
            file: "zon",
            columns: [["44S-24S", 7]],
            scaleValue: 1,
            kind: "temperature_difference",
        },
        {
            id: "lat6",
            name: "Latitudes 44&deg; S - 64&deg; S annual mean temperature",
            description: DESCRIPTION_GISTEMP,
            references: REFERENCES_GISTEMP,
            file: "zon",
            columns: [["64S-44S", 7]],
            scaleValue: 1,
            kind: "temperature_difference",
        },
        {
            id: "lat7",
            name: "Latitudes 64&deg; S - 90&deg; S annual mean temperature",
            description: DESCRIPTION_GISTEMP,
            references: REFERENCES_GISTEMP,
            file: "zon",
            columns: [["90S-64S", 7]],
            scaleValue: 1,
            kind: "temperature_difference",
        },
        {
            id: "pglob",
            name: "Global annual mean precipitation",
            description: DESCRIPTION_PRECIP_CRU,
            references: REFERENCES_PRECIP_CRU,
            file: "pa",
            yearColumn: 0,
            columns: [[1, 7]],
            scaleValue: mmpd,
            kind: "volumetric_flux",
            suggestedUnit: "mm/day",
        },
        {
            id: "plat0",
            name: "Latitudes 90&deg; N - 24&deg; N annual mean precipitation",
            description: DESCRIPTION_PRECIP_CRU,
            references: REFERENCES_PRECIP_CRU,
            file: "pb",
            yearColumn: 0,
            columns: [[1, 7]],
            scaleValue: mmpd,
            kind: "volumetric_flux",
            suggestedUnit: "mm/day",
        },
        {
            id: "plat1",
            name: "Latitudes 24&deg; N - 24&deg; S annual mean precipitation",
            description: DESCRIPTION_PRECIP_CRU,
            references: REFERENCES_PRECIP_CRU,
            file: "pb",
            yearColumn: 0,
            columns: [[2, 7]],
            scaleValue: mmpd,
            kind: "volumetric_flux",
            suggestedUnit: "mm/day",
        },
        {
            id: "plat2",
            name: "Latitudes 24&deg; S - 90&deg; S annual mean precipitation",
            description: DESCRIPTION_PRECIP_CRU,
            references: REFERENCES_PRECIP_CRU,
            file: "pb",
            yearColumn: 0,
            columns: [[3, 7]],
            scaleValue: mmpd,
            kind: "volumetric_flux",
            suggestedUnit: "mm/day",
        },
    ];

    var cache = {};

    function NASAGoddardDataset() {
        var self = this;
        self.seriesID = null;
        self.gregorianCalendar = new gregorian.GregorianCalendar();

        this.txtParse = function(data, startRow) {
            return {
                data: data
                    .split("\n")
                    .slice(startRow)
                    .filter(row => (!/^[- ]*$/.test(row)))
                    .map(row => row.split(" ").filter(cell => (cell != ""))),
            };
        };

        this.processData = function(series, table) {
            var yearCol;
            if ("yearColumn" in series) {
                yearCol = series.yearColumn;
            } else {
                yearCol = table.headings.findIndex(h => (h == "Year"));
            }
            var dataCols = series.columns.map(c => [
                Number.isInteger(c[0]) ? c[0] : table.headings.findIndex(h => (h == c[0])),
                c[1]]);
            var a = {
                type: 'num',
                dataset: self,
                kind: series.kind,
                data: [],
            };
            if ("suggestedUnit" in series) {
                a.suggestedUnit = series.suggestedUnit;
            }
            a.description = series.description;
            a.references = series.references;
            for (let row of table.data) {
                for (let col of dataCols) {
                    a.data.push({
                        time: self.gregorianCalendar.dateToTimestamp([row[yearCol], col[1], 1]),
                        value: series.scaleValue * parseFloat(row[col[0]]),
                    });
                }
            }
            return a;
        };
    };

    NASAGoddardDataset.prototype.getSeries = function() {
        return SERIES.map(s => ({ id: s.id, name: s.name }));
    };

    NASAGoddardDataset.prototype.setSeries = function(id) {
        var i = SERIES.findIndex(s => (s.id == id));
        if (i < 0) {
            console.warn("No series with id: " + id);
            return;
        }
        this.seriesID = id;
    };

    NASAGoddardDataset.prototype.fetch = function() {
        var self = this;
        return new Promise(function(resolve, reject) {
            var series = SERIES.find(s => (s.id == self.seriesID));
            var dataFile = DATA_FILES.find(f => (f.id == series.file));
            if (dataFile.id in cache) {
                resolve(self.processData(series, cache[dataFile.id]));
            } else {
                http.get(dataFile.url).then(function(fileContents) {
                    switch (dataFile.format) {
                    case 'csv':
                        cache[dataFile.id] = CSV.parse(fileContents, dataFile.headerRow, dataFile.headerRow + 1);
                        break;
                    case 'txt':
                        cache[dataFile.id] = self.txtParse(fileContents, dataFile.dataRow);
                        break;
                    }
                    resolve(self.processData(series, cache[dataFile.id]));
                });
            }
        });
    };

    return {
        NASAGoddardDataset: NASAGoddardDataset,
    };
});
