# Aeon

This web component provides a configurable timeline that can be instantiated on web sites and is configured and controlled with a JavaScript interface.

## Licensing

D3 Widgets is covered by the [LGPL v3](LICENSE) license, or any later version of that license.
