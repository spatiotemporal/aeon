// Adapted from this article:
// https://www.sitepoint.com/building-library-with-requirejs/

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['d3'], factory);
  } else {
    // Browser globals.
    root.aeon = factory(root.d3);
  }
}(window, function(d3) {
