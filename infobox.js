define(['require', 'd3'], function(require) {

    var d3 = require('d3');

    function InfoBox(element) {
        var self = this;
        if (typeof element == "string") {
            this.id = element;
            this.container = d3.select("#" + element);
        } else {
            if (!element.attr("id")) {
                console.error("Container element must have an id attribute");
                return;
            }
            this.id = element.attr("id");
            this.container = element;
        }
        self.currentObject = null;
        self.calendar = null;

        this.aeonConnectable = true;
        this.connectedObjects = new Set();

        self.showDate = function(timestamp) {
            if (self.calendar === null) {
                return null;
            }
            var str = "";
            var date = self.calendar.timestampToDate(timestamp);
            if (date) {
                date.forEach(function(t, unit) {
                    str += self.calendar.unitValueString(unit, t) + " ";
                });
            }
            return str.trim();
        };

        self.showResources = function(links) {
            html = "";
            if (links) { links.forEach(function(link) {
                if ("resource" in link && "url" in link) {
                    if (html) { html += "<br>"; }
                    html += "<a target=\"_blank\" href=\"" + link.url + "\">" + link.resource + "</a>";
                }
            })};
            return html || null;
        }


        self.fields = [
            {name: "Resources",  displayCallback: self.showResources, displayData: "links"},
            {name: "Start time", displayCallback: self.showDate, displayData: "start"},
            {name: "End time",   displayCallback: self.showDate, displayData: "end"},
        ];

        self.inner = self.container.append("div");
        self.header = self.inner.append("h1").attr("class", "aeon-infobox-headline");
        self.image = self.inner.append("img").attr("class", "aeon-infobox-image");
        self.description = self.inner.append("p").attr("class", "aeon-infobox-description");
        self.table = self.inner.append("table");

        self.addRow = function(name, data) {
            var dataRow = self.table.append("tr");
            dataRow.append("td").attr("class", "aeon-infobox-property-name").html(name + ":");
            var dataCell = dataRow.attr("class", "aeon-infobox-property-value").append("td");
            dataRow.style("display", "table-row");
            dataCell.html(data);
        }

        self.update = function() {
            if (self.currentObject === null) {
                self.container.attr("class", "aeon-infobox aeon-active");
                self.inner.style("display", "none");
                return;
            }

            self.container.attr("class", "aeon-infobox aeon-inactive");
            self.inner.style("display", "block");
            self.header.html(self.currentObject.title);
            self.description.html(self.currentObject.description);
            if (self.currentObject.image) {
                self.image.style("display", "block");
                self.image.attr("src", self.currentObject.image);
            } else {
                self.image.style("display", "none");
            }
            self.table.selectAll("*").remove();
            for (let field of self.fields) {
                var fieldData = field.displayCallback(self.currentObject[field.displayData]);
                if (fieldData) {
                    self.addRow(field.name, fieldData);
                }
            }
            if ("info_fragments" in self.currentObject) {
                for (let f of self.currentObject.info_fragments) {
                    self.addRow(f.name, f.fragment);
                }
            }
        }

        self.update();
    }

    InfoBox.prototype.showObject = function(obj) {
        var self = this;
        self.currentObject = obj;
        if (obj !== null && "more_info_cb" in obj) {
            obj.more_info_cb().then(function(info) {
                if (obj.id !== self.currentObject.id) {
                    return;
                }
                self.currentObject = {...self.currentObject, ...info};
                self.update();
            });
        }
        self.update();
    }

    InfoBox.prototype.setCalendar = function(calendar) {
        this.calendar = calendar;
        this.update();
    }

    InfoBox.prototype.event = function(call, arg, excludeSelf = false) {
        var event = {
            call: call,
            arg: arg,
            processedObjects: new Set(),
            excludeSelf: excludeSelf
        };
        this.propagateEvent(event);
    }

    InfoBox.prototype.propagateEvent = function(event) {
        event.processedObjects.add(this);
        if (event.excludeSelf) {
            event.excludeSelf = false;
        } else {
            if (event.call in this) {
                this[event.call](event.arg);
            }
        }
        event.processedObjects.add(this);
        this.connectedObjects.forEach(function(object) {
            if (!event.processedObjects.has(object)) {
                object.propagateEvent(event);
            }
        });
    }

    InfoBox.prototype.connect = function(object) {
        if (this.connectedObjects.has(object)) {
            return;
        }
        if ("aeonConnectable" in object && object.aeonConnectable) {
            this.connectedObjects.add(object);
            object.connect(this);
        } else {
            console.error("Not an Aeon connectable object");
        }
    }

    InfoBox.prototype.disconnect = function(object) {
        if (this.connectedObjects.delete(object)) {
            object.disconnect(this);
        }
    }

    return {
        InfoBox: InfoBox,
    };
});
