// Adapted from this article:
// https://www.sitepoint.com/building-library-with-requirejs/

  // Register in the values from the outer closure for common dependencies
  // as local almond modules
  define('d3', function() {
    return d3;
  });

  // Use almond's special top level synchronous require to trigger factory
  // functions, get the final module, and export it as the public api.
  return require('aeon');
}));
