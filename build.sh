#!/bin/bash

set -e

WRAPPERS="d3-selection.js d3-transition.js d3-interpolate.js d3-drag.js d3-dispatch.js"

mkdir -p build

# Combine all source files into one
for W in $WRAPPERS; do ln -sf d3-wrapper.js $W; done
node_modules/requirejs/bin/r.js -o requirejs.config.js
rm -f $WRAPPERS

# Transpile to browser-friendly JS
node_modules/@babel/cli/bin/babel.js build/aeon.full.js -o build/aeon.js

# Minify
node_modules/uglify-js/bin/uglifyjs build/aeon.js --compress --mangle > build/aeon.min.js
