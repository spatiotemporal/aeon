define(['require',
        './timeline/timeline', './infobox',
        './js-quantities/build/quantities',
        './data_sources/json',
        './data_sources/wikidata',
        './data_sources/who',
        './data_sources/noaa_cdo',
        './data_sources/nasa_goddard',
        './data_sources/themoviedb',
        './data_sources/discogs',
        './calendars/unix_epoch', './calendars/before_present', './calendars/astronomical_years',
        './calendars/julian', './calendars/byzantine', './calendars/gregorian', './calendars/gregorian_weekday',
        './calendars/egyptian', './calendars/hebrew'],
       function (require) {

           return {
               Timeline: require('./timeline/timeline').Timeline,
               InfoBox: require('./infobox').InfoBox,

               Qty: require('./js-quantities/build/quantities'),

               JSONDataset: require('./data_sources/json').JSONDataset,
               WikidataSet: require('./data_sources/wikidata').WikidataSet,
               WHODataset: require('./data_sources/who').WHODataset,
               NOAACDODataset: require('./data_sources/noaa_cdo').NOAACDODataset,
               NASAGoddardDataset: require('./data_sources/nasa_goddard').NASAGoddardDataset,
               TheMovieDBDataset: require('./data_sources/themoviedb').TheMovieDBDataset,
               DiscogsDataset: require('./data_sources/discogs').DiscogsDataset,

               UnixEpochCalendar: require('./calendars/unix_epoch').UnixEpochCalendar,
               BeforePresentCalendar: require('./calendars/before_present').BeforePresentCalendar,
               AstronomicalYearCalendar: require('./calendars/astronomical_years').AstronomicalYearCalendar,
               JulianCalendar: require('./calendars/julian').JulianCalendar,
               ByzantineCalendar: require('./calendars/byzantine').ByzantineCalendar,
               GregorianCalendar: require('./calendars/gregorian').GregorianCalendar,
               GregorianWeekdayCalendar: require('./calendars/gregorian_weekday').GregorianWeekdayCalendar,
               AncientEgyptianCalendar: require('./calendars/egyptian').AncientEgyptianCalendar,
               HebrewCalendar: require('./calendars/hebrew').HebrewCalendar,
           };
       });
