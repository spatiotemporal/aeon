define(['require', 'd3', 'd3-xyzoom'], function(require) {

    var d3 = require('d3');
    var d3_xyzoom = require('d3-xyzoom');

    function NumericDataRenderer(selectCallback) {
        var self = this;
    }

    ///////////////////////
    // Generic renderer API

    NumericDataRenderer.prototype.addDataset = function(ds) {
    }

    NumericDataRenderer.prototype.removeDataset = function(ds) {
    }

    NumericDataRenderer.prototype.render = function(ds, yScaleInfo) {
        var minValue = yScaleInfo.kinds[ds.kind].minValue;
        var maxValue = yScaleInfo.kinds[ds.kind].maxValue;
        ds.symbol.attr("stroke", ds.color).attr("fill", ds.color);
        var pointData = ds.svgGroup.selectAll("use").data(ds.data);
        pointData.enter().append("use")
            .attr("class", (ds.cssClass !== null)
                  ? ("aeon-datapoint " + ds.cssClass)
                  : "aeon-datapoint")
            .attr("href", "#" + ds.symbolID)
            .merge(pointData)
            .attr("y", o => (yScaleInfo.yMin
                             - ((o.value - minValue) / (maxValue - minValue)) * yScaleInfo.yHeight
                             - ds.plotPointSize / 2));
        pointData.exit().remove();
        var lineData = ds.svgGroup.selectAll("line").data(("pattern" in ds.plotLineStyle) ? ds.data.slice(1) : []);
        lineData.enter().append("line")
            .attr("class", (ds.cssClass !== null)
                  ? ("aeon-dataline " + ds.cssClass)
                  : "aeon-dataline")
            .merge(lineData)
            .attr("stroke", ds.color)
            .attr("stroke-width", ds.plotLineWidth)
            .attr("stroke-dasharray", ("pattern" in ds.plotLineStyle && ds.plotLineStyle.pattern !== null)
                  ? ds.plotLineStyle.pattern.map(x => ds.plotLineWidth * x).join(',') : "none")
            .attr("y1", (o, i) => (yScaleInfo.yMin
                                   - ((ds.data[i].value - minValue) / (maxValue - minValue)) * yScaleInfo.yHeight))
            .attr("y2", o => (yScaleInfo.yMin
                              - ((o.value - minValue) / (maxValue - minValue)) * yScaleInfo.yHeight));
        lineData.exit().remove();
    };

    NumericDataRenderer.prototype.transform = function(ds, T) {
        ds.svgGroup.selectAll("use").data(ds.data)
            .attr("x", o => (T.kx * o.time + T.x - ds.plotPointSize / 2));
        ds.svgGroup.selectAll("line").data(ds.data.slice(1))
            .attr("x1", (o, i) => (T.kx * ds.data[i].time + T.x))
            .attr("x2", o => (T.kx * o.time + T.x));
    }

    return {
        NumericDataRenderer: NumericDataRenderer,
    };
});
