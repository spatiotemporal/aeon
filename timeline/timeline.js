define(['require', 'd3', 'd3-xyzoom', './entity_renderer', './numeric_data_renderer', '../js-quantities/build/quantities', '../quantity_kinds'], function(require) {

    var d3 = require('d3');
    var d3_xyzoom = require('d3-xyzoom');
    var entityRenderer = require('./entity_renderer');
    var numericDataRenderer = require('./numeric_data_renderer');
    var Qty = require('../js-quantities/build/quantities');
    var qty_kinds = require('../quantity_kinds');

    const DEFAULT_MINIMUM_HEIGHT = 200;
    const DEFAULT_START_TIME = -2208988800;
    const DEFAULT_END_TIME = 946684800;
    const DEFAULT_MAX_START_TIME = -435485999735596800;
    const DEFAULT_MAX_END_TIME = 946684800;
    const DEFAULT_MAX_ZOOM_FACTOR = 1 / 3600;
    const DEFAULT_ENTITY_COLOR = "#C0C0C0";
    const DEFAULT_ENTITY_SECONDARY_COLOR = "#E0E0E0";
    const NUMBER_OF_AXES = 2;
    const NUMBER_OF_Y_AXES = 2;
    const AXIS_LABEL_TILT_ANGLE = 20;
    const AXIS_TICK_LEVEL_WIDTH = 1;
    const AXIS_TICK_BASE_HEIGHT = 1;
    const AXIS_TICK_LEVEL_HEIGHT = 3;
    const AXIS_TEXT_REL_MARGIN = [0, 0.6];
    const AXIS_MIN_TICK_DISTANCE = 80;
    const Y_AXIS_TICK_LENGTH = 4;
    const Y_AXIS_LABEL_MARGIN = 8;
    const Y_AXIS_MIN_TICK_DISTANCE = 30;
    const SCROLLER_WIDTH = 10;
    const DEFAULT_PLOT_POINT_SIZE = 10;
    const DEFAULT_PLOT_LINE_WIDTH = 1;
    const PLOT_MARGIN = 10;

    const PLOT_POINT_STYLES = [
        {
            name: "none",
            createSymbol: function(sym, size) {},
        },
        {
            name: "circle",
            createSymbol: function(sym, size) {
                sym.append("circle")
                    .attr("cx", size / 2)
                    .attr("cy", size / 2)
                    .attr("r", size / 2 - 1);
            },
        },
        {
            name: "hcircle",
            createSymbol: function(sym, size) {
                sym.append("circle")
                    .attr("fill-opacity", "0")
                    .attr("cx", size / 2)
                    .attr("cy", size / 2)
                    .attr("r", size / 2 - 1);
            },
        },
        {
            name: "square",
            createSymbol: function(sym, size) {
                sym.append("rect")
                    .attr("x", 1)
                    .attr("y", 1)
                    .attr("width", size - 2)
                    .attr("height", size - 2);
            },
        },
        {
            name: "hsquare",
            createSymbol: function(sym, size) {
                sym.append("rect")
                    .attr("fill-opacity", "0")
                    .attr("x", 1)
                    .attr("y", 1)
                    .attr("width", size - 2)
                    .attr("height", size - 2);
            },
        },
        {
            name: "triup",
            createSymbol: function(sym, size) {
                sym.append("polygon")
                    .attr("points", (size / 2) + ",1 0," + (size - 1) + " " + size + "," + (size - 1));
            },
        },
        {
            name: "htriup",
            createSymbol: function(sym, size) {
                sym.append("polygon")
                    .attr("fill-opacity", "0")
                    .attr("points", (size / 2) + ",1 0," + (size - 1) + " " + size + "," + (size - 1));
            },
        },
        {
            name: "tridown",
            createSymbol: function(sym, size) {
                sym.append("polygon")
                    .attr("points", "0,1 " + size + ",1 " + (size / 2) + "," + (size - 1));
            },
        },
        {
            name: "htridown",
            createSymbol: function(sym, size) {
                sym.append("polygon")
                    .attr("fill-opacity", "0")
                    .attr("points", "0,1 " + size + ",1 " + (size / 2) + "," + (size - 1));
            },
        },
        {
            name: "diamond",
            createSymbol: function(sym, size) {
                sym.append("polygon")
                    .attr("points", (size / 2) + ",0 " + size + "," + (size / 2) + " "
                          + (size / 2) + "," + size + " 0," + (size / 2));
            },
        },
        {
            name: "hdiamond",
            createSymbol: function(sym, size) {
                sym.append("polygon")
                    .attr("fill-opacity", "0")
                    .attr("points", (size / 2) + ",0 " + size + "," + (size / 2) + " "
                          + (size / 2) + "," + size + " 0," + (size / 2));
            },
        },
        {
            name: "plus",
            createSymbol: function(sym, size) {
                sym.append("path").attr("d", "M 1," + (size / 2) + " L " + (size - 1) + "," + (size / 2)
                                        + " M " + (size / 2) + ",1 L " + (size / 2) + "," + (size - 1));
            },
        },
        {
            name: "cross",
            createSymbol: function(sym, size) {
                sym.append("path").attr("d", "M 1,1 L " + (size - 1) + "," + (size - 1)
                                        + "M 1," + (size - 1) + " L " + (size - 1) + ",1");
            },
        },
        {
            name: "star",
            createSymbol: function(sym, size) {
                sym.append("path").attr("d", "M 1," + (size / 2) + " L " + (size - 1) + "," + (size / 2)
                                        + " M " + (size / 2) + ",1 L " + (size / 2) + "," + (size - 1)
                                        + "M 1,1 L " + (size - 1) + "," + (size - 1)
                                        + "M 1," + (size - 1) + " L " + (size - 1) + ",1");
            },
        },
    ];
    const DEFAULT_PLOT_POINT_STYLE = "circle";

    const PLOT_LINE_STYLES = [
        {
            name: "none",
        },
        {
            name: "solid",
            pattern: null,
        },
        {
            name: "dotted",
            pattern: [1, 4],
        },
        {
            name: "dashed",
            pattern: [4, 4],
        },
        {
            name: "dotdashed",
            pattern: [4, 4, 1, 4],
        },
    ];
    const DEFAULT_PLOT_LINE_STYLE = "solid";

    var datasetID = 0;

    function Timeline(element) {
        var self = this;
        if (typeof element == "string") {
            this.id = element;
            this.container = d3.select("#" + element);
        } else {
            if (!element.attr("id")) {
                console.error("Container element must have an id attribute");
                return;
            }
            this.id = element.attr("id");
            this.container = element;
        }
        this.svg = this.container.append("svg");
        this.svg.attr("class", "aeon-timeline").style("width", "100%").style("height", "100%")
            .on("click", function() { self.selectObject(null); });
        this.svg.append("rect").attr("class", "aeon-timeline-outer-area")
            .attr("width", "100%").attr("height", "100%")
            .attr("fill", "white");
        this.innerArea = this.svg.append("rect").attr("class", "aeon-timeline-inner-area");
        this.innerArea.attr("x", 0).attr("y", 0).attr("width", "100%")
            .attr("height", self.svg.node().getBoundingClientRect().height)
            .attr("fill", "white");
        this.innerAreaClip = this.svg.append("clipPath").attr("id", this.id + "-inner-area").append("rect");
        this.innerAreaClip.attr("x", 0).attr("y", 0).attr("width", "100%")
            .attr("height", self.svg.node().getBoundingClientRect().height);
        this.yAxisGroup = new Array(NUMBER_OF_Y_AXES).fill(null);
        this.yAxisGroup.forEach(function(grp, index) {
            self.yAxisGroup[index] = self.svg.append("g");
        });
        this.vscrollbar = this.svg.append("rect").attr("class", "aeon-scrollbar")
            .attr("y", 0)
            .attr("width", SCROLLER_WIDTH).attr("height", "100%")
            .attr("fill-opacity", "0");
        this.vscroller = this.svg.append("rect").attr("class", "aeon-scroller")
            .attr("width", SCROLLER_WIDTH)
            .attr("rx", SCROLLER_WIDTH / 2).attr("ry", SCROLLER_WIDTH / 2);
        this.marks = new Array(NUMBER_OF_AXES).fill(null);
        this.marks.forEach(function(mark, index) {
            self.marks[index] = self.svg.append("g");
        });
        this.datasets = [];
        this.calendar = new Array(NUMBER_OF_AXES).fill(null);
        this.axisLine = new Array(NUMBER_OF_AXES).fill(null);
        this.timeContextText = new Array(NUMBER_OF_AXES).fill(null);
        this.timeLabelHeight = new Array(NUMBER_OF_AXES).fill(null);
        this.timeLabelSpace = new Array(NUMBER_OF_AXES).fill(null);
        this.timeCtxHeight = new Array(NUMBER_OF_AXES).fill(null);
        this.minimumHeight = DEFAULT_MINIMUM_HEIGHT;
        this.maxStartTime = DEFAULT_MAX_START_TIME;
        this.maxEndTime = DEFAULT_MAX_END_TIME;
        this.maxZoomFactor = DEFAULT_MAX_ZOOM_FACTOR;
        this.timeLocked = false;
        this.autoAdjustHeight = false;
        this.yAxisKind = new Array(NUMBER_OF_Y_AXES).fill(null);
        this.yAxisUnit = new Array(NUMBER_OF_Y_AXES).fill(null);
        this.yAxisSpace = new Array(NUMBER_OF_Y_AXES).fill(0);
        this.yLabel = new Array(NUMBER_OF_Y_AXES).fill(null);

        this.aeonConnectable = true;
        this.connectedObjects = new Set();

        d3.select(window).on('resize', function() {
            self.updateExtents();
            self.transform((d3.zoomTransform(self.svg.node())));
        });

        this.isRealNumber = function(n) {
            return typeof n == "number" && !isNaN(n) && isFinite(n);
        }

        this.roundDecimals = function(value, decimals) {
            return Number.parseFloat(value).toFixed(decimals);
        }

        this.getTextDimensions = function(cssClass, str) {
            var txt = this.svg.append("text").attr("visibility", "hidden").attr("class", cssClass).attr("x", 0).attr("y", 0).html(str);
            var bbox = txt.node().getBoundingClientRect();
            txt.remove();
            return bbox;
        }

        this.updateExtents = function() {
            var bbox = self.svg.node().getBoundingClientRect();
            var ibox = self.calcInnerRegionBox();
            var totalHeight = self.entityRenderer.getTotalHeight();
            var xMin, xMax, yMin, yMax, xScaleRange;
            if (self.timeLocked) {
                var T = d3.zoomTransform(self.svg.node());
                xMin = - T.x / T.kx;
                xMax = (bbox.width - T.x) / T.kx;
                xScaleRange = [T.kx, T.kx];
            } else {
                xMin = self.maxStartTime;
                xMax = self.maxEndTime;
                xScaleRange = [0, self.maxZoomFactor];
            }
            if (totalHeight > ibox.height) {
                // Vertical zoom needed
                yMin = 0;
                yMax = totalHeight;
            } else {
                // No vertical zoom needed
                yMin = totalHeight - ibox.height;
                yMax = totalHeight;
            }
            self.zoomer.extent([[self.yAxisSpace[0], ibox.y], [bbox.width - self.yAxisSpace[1], ibox.bottom]]);
            self.zoomer.translateExtent([[xMin, yMin], [xMax, yMax]]);
            self.zoomer.scaleExtent([xScaleRange, [1, 1]]);

            var axisLineY;
            self.updateAxisPlacement();
            for (var axis = 0; axis < NUMBER_OF_AXES; axis++) {
                switch (axis) {
                case 0: axisLineY = bbox.height - self.timeLabelSpace[0] - 1.5 * self.timeCtxHeight[0]; break;
                case 1: axisLineY = self.timeLabelSpace[1] + self.timeCtxHeight[1]; break;
                }
                self.innerArea.attr("y", ibox.y).attr("height", ibox.height);
                self.innerAreaClip.attr("y", ibox.y).attr("height", ibox.height);
                self.vscrollbar.attr("y", ibox.y).attr("height", ibox.height);
                if (self.calendar[axis] !== null) {
                    self.axisLine[axis]
                        .attr("y1", axisLineY)
                        .attr("y2", axisLineY);
                }
            }
        }

        this.calcInnerRegionBox = function() {
            var bbox = self.svg.node().getBoundingClientRect();
            var ibox = {y: 0, height: bbox.height, bottom: bbox.height};
            if (self.calendar[0] !== null) {
                ibox.height -= (self.timeLabelSpace[0] + 1.5 * self.timeCtxHeight[0]);
                ibox.bottom -= (self.timeLabelSpace[0] + 1.5 * self.timeCtxHeight[0]);
            }
            if (self.calendar[1] !== null) {
                ibox.y = (self.timeLabelSpace[1] + self.timeCtxHeight[1]);
                ibox.height -= (self.timeLabelSpace[1] + self.timeCtxHeight[1]);
            }
            return ibox;
        }

        this.updateTimeAxis = function(axis, T, bbox) {
            // One pixel represents T.kx seconds.
            var marks = [];
            var ctxTextElements = [];

            if (self.calendar[axis] !== null) {

                var units = self.calendar[axis].getUnits();
                var firstTimestamp = (- T.x - AXIS_MIN_TICK_DISTANCE) / T.kx;
                var lastTimestamp = (bbox.width - T.x) / T.kx;
                var startDate = self.calendar[axis].timestampToDate((- T.x + self.yAxisSpace[0]) / T.kx);

                // Find finest unit to show and step size for that unit.
                var step, s, cutoffUnit = 0, indNum = null;
                for (var i = 0; i < units.length; i++) {
                    var indicativeSeconds = 1;
                    for (var j = i; j < units.length; j++) {
                        indicativeSeconds *= units[j].indicativeLength;
                    }

                    if (startDate !== undefined && (i == 0 || bbox.width / T.kx < 2 * indicativeSeconds)) {
                        ctxTextElements.push(self.calendar[axis].unitValueString(i, startDate[i]));
                    }

                    switch (units[i].periodization) {
                    case "decimal":
                        var dstep = 1, mstep = 1;
                        while (dstep * mstep * indicativeSeconds * T.kx < AXIS_MIN_TICK_DISTANCE) {
                            switch (mstep) {
                            case 1: mstep = 2; break;
                            case 2: mstep = 5; break;
                            case 5: mstep = 1; dstep *= 10; break;
                            }
                        }
                        s = dstep * mstep;
                        break;
                    case "ternary":
                        if (1 * indicativeSeconds * T.kx >= AXIS_MIN_TICK_DISTANCE) {
                            s = 1;
                        } else if (3 * indicativeSeconds * T.kx >= AXIS_MIN_TICK_DISTANCE) {
                            s = 3;
                        } else {
                            s = null;
                        }
                        break;
                    default:
                        s = 1;
                        while (s * indicativeSeconds * T.kx < AXIS_MIN_TICK_DISTANCE) {
                            s *= 2;
                        }
                        break;
                    }

                    if (s === null || (indNum !== null && s > indNum / 2)) {
                        break;
                    }

                    indNum = units[i].indicativeLength;
                    cutoffUnit = i;
                    step = s;
                }

                // Determine first mark to show.
                var range = self.calendar[axis].getValidRange();
                var markDate;
                if (range[1] && firstTimestamp > range[1]) {
                    markDate = self.calendar[axis].timestampToDate(range[1]);
                } else if (range[0] && firstTimestamp < range[0]) {
                    markDate = self.calendar[axis].rectify();
                } else {
                    markDate = self.calendar[axis].timestampToDate(firstTimestamp).map(function(value, index) {
                        return (index >= cutoffUnit && index > 0) ? 0 : value;
                    });
                    if (cutoffUnit == 0) {
                        markDate[0] = step * Math.floor(markDate[0] / step);
                    }
                    markDate = self.calendar[axis].rectify(markDate);
                }

                var markTimestamp = null;
                var offset = (cutoffUnit > 0) ? markDate[cutoffUnit] : 0;
                var largestChangedUnit = 0;
                do {
                    var ts = self.calendar[axis].dateToTimestamp(markDate);
                    if (markTimestamp !== null && ts - markTimestamp < 0.75 * AXIS_MIN_TICK_DISTANCE / T.kx) {
                        // Last mark was too close, remove it. This can happen when the cutoff unit wraps
                        // and a larger unit is incremented.
                        marks.pop();
                    }
                    markTimestamp = ts;
                    var markX = T.kx * markTimestamp + T.x;
                    marks.push({ text: self.calendar[axis].unitValueString(largestChangedUnit, markDate[largestChangedUnit]),
                                 timestamp: markTimestamp, unit: largestChangedUnit,
                                 onAxis: markX >= self.yAxisSpace[0] && markX <= bbox.width - self.yAxisSpace[1]});
                    var prevDate = markDate.slice();
                    markDate[cutoffUnit] = offset + step * (Math.floor((markDate[cutoffUnit] - offset) / step) + 1);
                    markDate = self.calendar[axis].rectify(markDate);
                    if (markDate.every((u, i) => (u == prevDate[i] || i > cutoffUnit))) {
                        // Cannot get further, assuming we are outside calendar's valid range.
                        break;
                    }
                    largestChangedUnit = markDate.findIndex(function(value, index) {
                        return value != prevDate[index];
                    });
                    largestChangedUnit = (largestChangedUnit >= 0) ? largestChangedUnit : cutoffUnit;
                    if (largestChangedUnit > cutoffUnit) {
                        offset = (cutoffUnit > 0) ? markDate[cutoffUnit] : 0;
                    }
                } while (markTimestamp < lastTimestamp);

            }

            var lineData = self.marks[axis].selectAll("line").data(marks);
            var textData = self.marks[axis].selectAll("text").data(marks);
            var axisTextY, axisLineY, axisTickDir, ctxTextY;
            switch (axis) {
            case 0:
                ctxTextY = bbox.height - self.timeCtxHeight[0] / 2;
                axisTextY = bbox.height - self.timeLabelSpace[0] - 1.5 * self.timeCtxHeight[0] + (1 + AXIS_TEXT_REL_MARGIN[0]) * self.timeLabelHeight[0];
                axisLineY = bbox.height - self.timeLabelSpace[0] - 1.5 * self.timeCtxHeight[0];
                axisTickDir = +1;
                break;
            case 1:
                ctxTextY = self.timeCtxHeight[1];
                axisTextY = self.timeLabelSpace[1] + self.timeCtxHeight[1] - AXIS_TEXT_REL_MARGIN[1] * self.timeLabelHeight[1];
                axisLineY = self.timeLabelSpace[1] + self.timeCtxHeight[1];
                axisTickDir = -1;
                break;
            }
            lineData.enter().append("line")
                .attr("stroke", "black")
                .merge(lineData)
                .attr("class", o => ("aeon-" + (axis ? "upper" : "lower") + "-axis aeon-tick aeon-tick-unit" + o.unit))
                .attr("x1", o => (T.kx * o.timestamp + T.x))
                .attr("x2", o => (T.kx * o.timestamp + T.x))
                .attr("y1", axisLineY)
                .attr("y2", o => (axisLineY + axisTickDir
                                  * (AXIS_TICK_BASE_HEIGHT + AXIS_TICK_LEVEL_HEIGHT * (units.length - o.unit))))
                .attr("visibility", o => (o.onAxis ? "visible" : "hidden"));
            lineData.exit().remove();
            textData.enter().append("text")
                .merge(textData)
                .attr("class", o => ("aeon-" + (axis ? "upper" : "lower") + "-axis aeon-tick-label aeon-tick-label-unit" + o.unit))
                .attr("x", o => (T.kx * o.timestamp + T.x))
                .attr("y", axisTextY)
                .attr("transform", o => (o.unit > 0 ? "" : ("rotate(" + axisTickDir * AXIS_LABEL_TILT_ANGLE + "," + (T.kx * o.timestamp + T.x) + "," + axisTextY + ")")))
                .html(o => o.text);
            textData.exit().remove();

            if (self.timeContextText[axis] !== null) {
                self.timeContextText[axis]
                    .attr("y", ctxTextY)
                    .html(ctxTextElements.join(' '));
            }
        }

        this.updateYAxis = function(axis, minValue, maxValue) {
            var bbox = self.svg.node().getBoundingClientRect();
            var ibox = self.calcInnerRegionBox();
            var g = self.yAxisGroup[axis];
            g.selectAll("*").remove();

            if (minValue === null || maxValue === null) {
                self.yAxisSpace[axis] = 0;
                if (axis) {
                    self.innerAreaClip.attr("width", bbox.width);
                } else {
                    self.innerAreaClip.attr("x", 0);
                }
                return;
            }

            var decimals = 0;
            var minInc = Y_AXIS_MIN_TICK_DISTANCE * (maxValue - minValue) / bbox.height;
            var exponent = Math.floor(Math.log10(minInc));
            if (exponent < 0) {
                decimals = Math.abs(exponent);
            }
            var inc = 10 ** Math.floor(Math.log10(minInc));
            for (let n of [2, 5, 10]) {
                if (n * inc >= minInc) {
                    inc *= n;
                    if (n == 10 && decimals > 0) {
                        decimals--;
                    }
                    break;
                }
            }

            var txtClass = "aeon-" + (axis ? "right" : "left") + "-axis aeon-tick-label";
            var lblClass = "aeon-" + (axis ? "right" : "left") + "-axis aeon-axis-label";
            var maxChars = 0;
            for (var value = inc * Math.floor(minValue / inc); value <= maxValue; value += inc) {
                maxChars = Math.max(maxChars, self.roundDecimals(value, decimals).toString().length);
            }
            var axisSpace = Y_AXIS_TICK_LENGTH + Y_AXIS_LABEL_MARGIN
                + self.getTextDimensions(txtClass, "0".repeat(maxChars)).width;

            var labelHeight = 0;
            if (self.yLabel[axis] !== null) {
                labelHeight = self.getTextDimensions(lblClass, self.yLabel[axis]).height
                axisSpace += labelHeight;
            }

            var x, xm, xt, xl, labelRot, anchor;
            switch (axis) {
            case 0:
                x = axisSpace;
                xm = x - Y_AXIS_TICK_LENGTH;
                xt = xm - Y_AXIS_LABEL_MARGIN;
                xl = labelHeight / 2;
                labelRot = -90;
                anchor = "end";
                self.innerAreaClip.attr("x", axisSpace);
                break;
            case 1:
                x = bbox.width - axisSpace;
                xm = x + Y_AXIS_TICK_LENGTH;
                xt = xm + Y_AXIS_LABEL_MARGIN;
                xl = bbox.width - labelHeight / 2;
                labelRot = 90;
                anchor="start";
                self.innerAreaClip.attr("width", bbox.width - self.innerAreaClip.attr("x") - axisSpace);
                break;
            }

            if (self.yLabel[axis] !== null) {
                g.append("text")
                    .attr("class", lblClass)
                    .attr("x", xl)
                    .attr("y", ibox.y + ibox.height / 2)
                    .attr("text-anchor", "middle")
                    .attr("dominant-baseline", "middle")
                    .attr("transform", "rotate(" + labelRot + "," + xl + "," + (ibox.y + ibox.height / 2) + ")")
                    .html(self.yLabel[axis]);
            }

            g.append("line")
                .attr("stroke", "black")
                .attr("class", "aeon-" + (axis ? "right" : "left") + "-axis aeon-axis-line")
                .attr("x1", x)
                .attr("x2", x)
                .attr("y1", ibox.y)
                .attr("y2", ibox.y + ibox.height);

            for (var value = inc * Math.ceil(minValue / inc); value <= maxValue; value += inc) {
                var y = ibox.y + ibox.height - PLOT_MARGIN
                    - ((value - minValue) / (maxValue - minValue))
                    * (ibox.height - 2 * PLOT_MARGIN);
                g.append("line")
                    .attr("stroke", "black")
                    .attr("class", "aeon-" + (axis ? "right" : "left") + "-axis aeon-tick")
                    .attr("x1", x)
                    .attr("x2", xm)
                    .attr("y1", y)
                    .attr("y2", y);
                g.append("text")
                    .attr("class", txtClass)
                    .attr("x", xt)
                    .attr("y", y)
                    .attr("dominant-baseline", "middle")
                    .attr("text-anchor", anchor)
                    .html(self.roundDecimals(value, decimals).toString());
            }

            self.yAxisSpace[axis] = axisSpace;
        }

        this.updateVerticalScrollbar = function(y) {
            var ibox = self.calcInnerRegionBox();
            var totalHeight = self.entityRenderer.getTotalHeight();
            if (totalHeight <= ibox.height) {
                self.vscrollbar.attr("visibility", "hidden");
                self.vscroller.attr("visibility", "hidden");
            } else {
                self.vscrollbar.attr("visibility", "visible")
                    .attr("x", this.svg.node().getBoundingClientRect().width - SCROLLER_WIDTH)
                self.vscroller.attr("visibility", "visible")
                    .attr("x", this.svg.node().getBoundingClientRect().width - SCROLLER_WIDTH)
                    .attr("y", ibox.y
                          + (ibox.y * (ibox.height - ibox.height**2 / totalHeight)) / (totalHeight - ibox.height)
                          + y * ibox.height / totalHeight)
                    .attr("height", ibox.height**2 / totalHeight);
            }
        }

        this.estimateLabelBounds = function(calendar, textClass) {
            var range = calendar.getValidRange();
            var str, labelBounds;
            if (DEFAULT_MAX_START_TIME < range[0]) {
                str = calendar.timestampToDate(range[0]);
            } else {
                str = calendar.unitValueString(0, calendar.timestampToDate(DEFAULT_MAX_START_TIME)[0]);
            }
            b1 = this.getTextDimensions(textClass, str);
            if (DEFAULT_MAX_END_TIME > range[1]) {
                str = calendar.timestampToDate(range[1]);
            } else {
                str = calendar.unitValueString(0, calendar.timestampToDate(DEFAULT_MAX_END_TIME)[0]);
            }
            b2 = this.getTextDimensions(textClass, str);

            return {
                width: Math.max(b1.width, b2.width),
                height: Math.max(b1.height, b2.height),
            };
        }

        this.adjustHeight = function() {
            if (self.autoAdjustHeight) {
                var totalHeight = self.entityRenderer.getTotalHeight();
                var height = Math.max(totalHeight, self.minimumHeight);
                for (var i = 0; i < NUMBER_OF_AXES; i++) {
                    height += self.timeLabelSpace[i] + 1.5 * self.timeCtxHeight[i];
                }
                self.svg.style("height", Math.ceil(height) + "px");
            }
            this.updateExtents();
        }

        this.zoomHandler = function() {
            var bbox = self.svg.node().getBoundingClientRect();
            var T = d3.event.transform;
            self.transform(T);
            self.event("setTimeRangeEvent", { startTime: - T.x / T.kx, endTime: (bbox.width - T.x) / T.kx }, true);
        }

        this.transform = function(T) {
            var bbox = self.svg.node().getBoundingClientRect();
            var ibox = self.calcInnerRegionBox();
            for (var i = 0; i < self.datasets.length; i++) {
                self.datasets[i].renderer.transform(self.datasets[i], T, ibox);
            }
            for (var i = 0; i < NUMBER_OF_AXES; i++) {
                self.updateTimeAxis(i, T, bbox);
            }
            self.updateVerticalScrollbar(- T.y);
        }

        this.selectObject = function(e) {
            self.event("showObject", e);
            d3.event.stopPropagation();
        }

        this.calcYScaleInfo = function() {
            var kinds = {};
            var ibox = self.calcInnerRegionBox();

            for (let ds of this.datasets) {
                if (!(ds.kind in kinds)) {
                    var datasetsWithSameKind = this.datasets.filter(d => (d.type == 'num' && d.kind == ds.kind));
                    var minValue = Math.min.apply(Math, datasetsWithSameKind.map(d => (d.minValue)));
                    var maxValue = Math.max.apply(Math, datasetsWithSameKind.map(d => (d.maxValue)));
                    if (!self.isRealNumber(minValue) || !self.isRealNumber(maxValue)) {
                        minValue = -1;
                        maxValue = +1;
                    } else if (maxValue <= minValue) {
                        minValue = (minValue + maxValue) / 2 - 1;
                        maxValue = (minValue + maxValue) / 2 + 1;
                    }
                    kinds[ds.kind] = { minValue: minValue, maxValue: maxValue };
                }
            }

            return {
                kinds: kinds,
                yMin: ibox.y + ibox.height - PLOT_MARGIN,
                yHeight: ibox.height - 2 * PLOT_MARGIN,
            };
        }

        this.updateDOM = function() {
            var bbox = this.svg.node().getBoundingClientRect();
            var yScaleInfo = self.calcYScaleInfo();
            for (var i = 0; i < this.datasets.length; i++) {
                self.datasets[i].renderer.render(self.datasets[i], yScaleInfo);
            }

            for (var i = 0; i < NUMBER_OF_Y_AXES; i++) {
                var minValue = null, maxValue = null;
                if (this.yAxisKind[i] !== null) {
                    if (this.yAxisKind[i] in yScaleInfo.kinds) {
                        minValue = Qty(yScaleInfo.kinds[this.yAxisKind[i]].minValue,
                                       qty_kinds.QUANTITY_KINDS[this.yAxisKind[i]])
                            .to(this.yAxisUnit[i]).scalar;
                        maxValue = Qty(yScaleInfo.kinds[this.yAxisKind[i]].maxValue,
                                       qty_kinds.QUANTITY_KINDS[this.yAxisKind[i]])
                            .to(this.yAxisUnit[i]).scalar;
                    } else {
                        minValue = 0;
                        maxValue = 1;
                    }
                }
                this.updateYAxis(i, minValue, maxValue);
            }

            for (var i = 0; i < NUMBER_OF_AXES; i++) {
                if (this.calendar[i] !== null) {
                    this.axisLine[i]
                        .attr("x1", this.yAxisSpace[0])
                        .attr("x2", bbox.width - this.yAxisSpace[1]);
                    this.timeContextText[i].attr("x", this.yAxisSpace[0]);
                }
            }



            this.transform(d3.zoomTransform(this.svg.node()));
            this.updateExtents();
            this.zoomer.translateBy(this.svg, 0, 0);
        };

        this.setTimeRangeEvent = function(range) {
            var T = d3.zoomTransform(this.svg.node());
            var bbox = this.svg.node().getBoundingClientRect();
            T.kx = bbox.width / (range.endTime - range.startTime);
            T.x = range.startTime * bbox.width / (range.startTime - range.endTime);
            this.transform(T);
        };

        this.getDatasetByObj = function(obj) {
            if (typeof obj == "object" && "type" in obj) {
                return this.datasets.find(s => s.dataset == obj.dataset);
            } else if (typeof obj == "object" && "fetch" in obj) {
                return this.datasets.find(s => s.dataset == obj);
            } else {
                return undefined;
            }
        };

        this.initDataset = function(dataset) {
            var s = {
                id: datasetID++,
                dataset: dataset,
                data: [],
                cssClass: null,
                color: DEFAULT_ENTITY_COLOR,
                secondaryColor: DEFAULT_ENTITY_SECONDARY_COLOR,
                entityStyle: this.entityRenderer.getDefaultStyle(),
                svgGroup: this.svg.append("g"),
                defGroup: this.svg.append("defs"),
                removed: false,
            };
            s.svgGroup.attr("clip-path", "url(#" + this.id + "-inner-area)")
            this.datasets.push(s);
            return s;
        }

        this.addResult = function(s, res) {
            if (!s.removed) {
                s.type = res.type;
                s.data = res.data;
                switch (res.type) {
                case 'entity': s.renderer = self.entityRenderer; break;
                case 'num': s.renderer = self.numericDataRenderer; break;
                }
                s.renderer.addDataset(s);
                switch (s.type) {
                case 'num':
                    s.kind = res.kind;
                    s.minValue = Math.min.apply(Math, s.data.map(o => o.value));
                    s.maxValue = Math.max.apply(Math, s.data.map(o => o.value));
                    s.symbolID = "aeon-plot-symbol-" + datasetID;
                    s.symbol = s.svgGroup.append("symbol")
                        .attr("id", s.symbolID);
                    s.plotPointSize = DEFAULT_PLOT_POINT_SIZE;
                    s.plotLineStyle = PLOT_LINE_STYLES.find(ps => ps.name == DEFAULT_PLOT_LINE_STYLE);
                    s.plotLineWidth = DEFAULT_PLOT_LINE_WIDTH;
                    PLOT_POINT_STYLES.find(ps => ps.name == DEFAULT_PLOT_POINT_STYLE).createSymbol(s.symbol, s.plotPointSize);
                    break;
                }
                self.datasets.filter(d => d.type === 'num').forEach(function(dataset) {
                    dataset.svgGroup.raise();
                });
                self.vscrollbar.raise();
                self.vscroller.raise();
                self.adjustHeight();
                self.updateDOM();
            }
        }

        this.entityRenderer = new entityRenderer.EntityRenderer(this.selectObject);
        this.numericDataRenderer = new numericDataRenderer.NumericDataRenderer(this.selectObject);
        this.entityRenderer.setMaxTimeRange(this.maxStartTime, this.maxEndTime);

        this.zoomer = d3_xyzoom.xyzoom().scaleRatio([1, 0]).on("zoom", this.zoomHandler);
        this.zoomer(this.svg);
        this.updateExtents();
        this.setTimeRangeEvent({ startTime: DEFAULT_START_TIME, endTime: DEFAULT_END_TIME });
    }

    Timeline.prototype.addDataset = function(obj) {
        var self = this;
        if (this.getDatasetByObj(obj) !== undefined) {
            console.warn("Dataset already added to timeline");
            return;
        }
        if (typeof obj == "object" && "type" in obj) {
            var s = self.initDataset(obj.dataset);
            self.addResult(s, obj);
        } else if (typeof obj == "object" && "fetch" in obj) {
            var s = self.initDataset(obj);
            obj.fetch().then(function(res) {
                self.addResult(s, res);
            });
        } else {
            console.error("Argument must be either a Dataset or a Result object");
        }
    };

    Timeline.prototype.removeDataset = function(obj) {
        var ds = this.getDatasetByObj(obj);
        if (ds === undefined) {
            console.warn("Dataset not found in timeline");
            return;
        }
        ds.data = [];
        ds.removed = true;
        ds.renderer.removeDataset(ds);
        this.datasets = this.datasets.filter(s => s.dataset != ds.dataset);
        this.adjustHeight();
        this.updateDOM();
        ds.svgGroup.remove();
        ds.defGroup.remove();
    }

    Timeline.prototype.updateDataset = function(obj) {
        var ds = this.getDatasetByObj(obj);
        if (ds === undefined) {
            console.warn("Dataset not found in timeline");
            return;
        }
        var cssClass = ds.cssClass;
        var color = ds.color;
        var secondaryColor = ds. secondaryColor;
        this.removeDataset(obj);
        this.addDataset(obj);
        this.setDatasetClass(obj, cssClass);
        this.setDatasetColor(obj, color, secondaryColor);
    }

    Timeline.prototype.setDatasetClass = function(obj, cssClass) {
        var ds = this.getDatasetByObj(obj);
        if (ds === undefined) {
            console.warn("Dataset not found in timeline");
            return;
        }
        ds.cssClass = cssClass;
        this.updateDOM();
    }

    Timeline.prototype.setDatasetColor = function(obj, color, secondaryColor) {
        var ds = this.getDatasetByObj(obj);
        if (ds === undefined) {
            console.warn("Dataset not found in timeline");
            return;
        }
        ds.color = color;
        if (secondaryColor !== undefined) {
            ds.secondaryColor = secondaryColor;
        }
        this.updateDOM();
    }

    Timeline.prototype.setDatasetEntityStyle = function(obj, style) {
        var ds = this.getDatasetByObj(obj);
        if (ds === undefined) {
            console.warn("Dataset not found in timeline");
            return;
        }
        this.entityRenderer.setStyle(ds, style);
        this.adjustHeight();
        this.updateDOM();
    }

    Timeline.prototype.setDatasetPlotStyle = function(obj, pointStyle, lineStyle) {
        var ds = this.getDatasetByObj(obj);
        var pps = PLOT_POINT_STYLES.find(ps => ps.name == pointStyle.split(':')[0]);
        var pls = PLOT_LINE_STYLES.find(ps => ps.name == lineStyle.split(':')[0]);
        if (ds === undefined) {
            console.warn("Dataset not found in timeline");
            return;
        }
        if (pps === undefined) {
            console.warn("Unknown point style '" + pointStyle.split(':')[0] + "'");
            return;
        }
        if (pls === undefined) {
            console.warn("Unknown line style '" + pointStyle.split(':')[0] + "'");
            return;
        }
        if (pointStyle.includes(":")) {
            ds.plotPointSize = pointStyle.split(':', 2)[1]
        } else {
            ds.plotPointSize = DEFAULT_PLOT_POINT_SIZE;
        }
        if (lineStyle.includes(":")) {
            ds.plotLineWidth = lineStyle.split(':', 2)[1]
        } else {
            ds.plotLineWidth = DEFAULT_PLOT_LINE_WIDTH;
        }

        d3.selectAll(ds.symbol.node().childNodes).remove();
        pps.createSymbol(ds.symbol, ds.plotPointSize);
        ds.plotLineStyle = pls;
        this.updateDOM();
    }

    Timeline.prototype.setTimeRange = function(startTime, endTime) {
        this.event("setTimeRangeEvent", { startTime: startTime, endTime: endTime });
    }

    Timeline.prototype.lockTimeRange = function() {
        this.timeLocked = true;
        this.updateExtents();
    }

    Timeline.prototype.unlockTimeRange = function() {
        this.timeLocked = false;
        this.updateExtents();
    }

    Timeline.prototype.setMaxTimeRange = function(maxStartTime, maxEndTime) {
        if (maxStartTime === null) {
            this.maxStartTime = DEFAULT_MAX_START_TIME;
        } else {
            this.maxStartTime = maxStartTime;
        }
        if (maxEndTime === null) {
            this.maxEndTime = DEFAULT_MAX_END_TIME;
        } else {
            this.maxEndTime = maxEndTime;
        }
        this.entityRenderer.setMaxTimeRange(this.maxStartTime, this.maxEndTime);
        this.updateExtents();
    }

    Timeline.prototype.setMaxZoomFactor = function(maxZoomFactor) {
        this.maxZoomFactor = maxZoomFactor;
        this.updateExtents();
    }

    Timeline.prototype.setEntityHeight = function(height) {
        this.entityRenderer.setEntityHeight(height);
        this.adjustHeight();
        this.updateDOM();
    }

    Timeline.prototype.setAutoAdjustHeight = function(autoAdjustEnabled, minimumHeight) {
        this.autoAdjustHeight = autoAdjustEnabled;
        if (minimumHeight === undefined) {
            this.minimumHeight = DEFAULT_MINIMUM_HEIGHT;
        } else {
            this.minimumHeight = minimumHeight;
        }
        this.adjustHeight();
        this.transform(d3.zoomTransform(this.svg.node()));
    }

    Timeline.prototype.setDeformOnZoomOut = function(deformEnabled) {
        this.entityRenderer.setDeformOnZoomOut(deformEnabled);
        this.transform(d3.zoomTransform(this.svg.node()));
    }

    Timeline.prototype.updateAxisPlacement = function() {
        for (var i = 0; i < NUMBER_OF_AXES; i++) {
            if (this.calendar[i] !== null) {
                var labelBounds = this.estimateLabelBounds(
                    this.calendar[i],
                    "aeon-" + (i ? "upper" : "lower") + "-axis aeon-tick-label aeon-tick-label-unit0");
                var contextBounds = this.estimateLabelBounds(
                    this.calendar[i],
                    "aeon-" + (i ? "upper" : "lower") + "-axis aeon-context-time-label");
                this.timeLabelHeight[i] = labelBounds.height;
                this.timeLabelSpace[i] = Math.ceil(
                    AXIS_TEXT_REL_MARGIN[i] * labelBounds.height
                        + labelBounds.width * Math.sin(AXIS_LABEL_TILT_ANGLE * Math.PI / 180)
                        + labelBounds.height * Math.cos(AXIS_LABEL_TILT_ANGLE * Math.PI / 180));
                this.timeCtxHeight[i] = Math.ceil(contextBounds.height);
            } else {
                this.timeLabelHeight[i] = 0;
                this.timeLabelSpace[i] = 0;
                this.timeCtxHeight[i] = 0;
            }
        }
    }

    Timeline.prototype.setCalendar = function(axis, calendar) {
        if (axis !== 0 && axis !== 1) {
            console.error("Illegal axis number (only 0 and 1 allowed)");
            return;
        }
        this.calendar[axis] = calendar;
        if (this.axisLine[axis] !== null) {
            this.axisLine[axis].remove();
            this.axisLine[axis] = null;
        }
        if (this.timeContextText[axis] !== null) {
            this.timeContextText[axis].remove();
            this.timeContextText[axis] = null;
        }
        if (calendar) {
            this.axisLine[axis] = this.svg.append("line")
                .attr("class", o => ("aeon-" + (axis ? "upper" : "lower") + "-axis aeon-axis-line"))
                .attr("stroke", "black");
            this.timeContextText[axis] = this.svg.append("text")
                .attr("class", "aeon-" + (axis ? "upper" : "lower") + "-axis aeon-context-time-label");
        }
        this.updateAxisPlacement();
        this.adjustHeight();
        this.updateDOM();
    }

    Timeline.prototype.setYAxis = function(axis, kind, unit) {
        this.yAxisKind[axis] = kind;
        this.yAxisUnit[axis] = unit;
        this.updateDOM();
    }

    Timeline.prototype.setYLabel = function(axis, label) {
        this.yLabel[axis] = label;
        this.updateDOM();
    }

    Timeline.prototype.event = function(call, arg, excludeSelf = false) {
        var event = {
            call: call,
            arg: arg,
            processedObjects: new Set(),
            excludeSelf: excludeSelf
        };
        this.propagateEvent(event);
    }

    Timeline.prototype.propagateEvent = function(event) {
        event.processedObjects.add(this);
        if (event.excludeSelf) {
            event.excludeSelf = false;
        } else {
            if (event.call in this) {
                this[event.call](event.arg);
            }
        }
        event.processedObjects.add(this);
        this.connectedObjects.forEach(function(object) {
            if (!event.processedObjects.has(object)) {
                object.propagateEvent(event);
            }
        });
    }

    Timeline.prototype.connect = function(object) {
        if (this.connectedObjects.has(object)) {
            return;
        }
        if ("aeonConnectable" in object && object.aeonConnectable) {
            this.connectedObjects.add(object);
            object.connect(this);
        } else {
            console.error("Not an Aeon connectable object");
        }
    }

    Timeline.prototype.disconnect = function(object) {
        if (this.connectedObjects.delete(object)) {
            object.disconnect(this);
        }
    }

    Timeline.getPlotPointStyles = function(object) {
        return PLOT_POINT_STYLES;
    }

    Timeline.getPlotLineStyles = function(object) {
        return PLOT_LINE_STYLES;
    }

    return {
        Timeline: Timeline,
    };
});
