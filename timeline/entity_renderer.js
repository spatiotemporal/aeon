define(['require', 'd3', 'd3-xyzoom'], function(require) {

    var d3 = require('d3');
    var d3_xyzoom = require('d3-xyzoom');

    const MOMENTARY_ENTITY_MAX_WIDTH = 200;
    const MOMENTARY_ENTITY_VISUAL_TIME = 365 * 24 * 60 * 60;
    const IMAGE_MARGIN = 4;
    const DEFAULT_ENTITY_HEIGHT = 20;

    const styles = [
        {
            name: "sub-box-interior",
            spacePerParent: 1,
            spacePerChild: 1,
            render: function(self, ds, yScaleInfo) {
                ds.data = ds.data.sort((a, b) => ((("parent" in a) ? 1 : 0) - (("parent" in b) ? 1 : 0)))
                var mainData = ds.svgGroup.selectAll("g").data(ds.data);
                var defsData = ds.defGroup.selectAll("clipPath").data(ds.data);
                var g = mainData.enter().append("g");
                g.filter(o => (o.start != o.end)).append("rect")
                    .attr("class", (ds.cssClass !== null)
                          ? ("aeon-entity " + ds.cssClass)
                          : "aeon-entity")
                    .attr("height", o => (o.size * self.entityHeight))
                    .attr("aeon:order", o => o.start)
                    .attr("fill", o => (("parent" in o) ? ds.secondaryColor : ds.color))
                    .attr("stroke", "black")
                    .attr("stroke-width", 1)
                    .on("click", self.selectCallback);
                g.filter(o => (o.start == o.end)).append("path")
                    .attr("class", (ds.cssClass !== null)
                          ? ("aeon-entity " + ds.cssClass)
                          : "aeon-entity")
                    .attr("aeon:order", o => o.start)
                    .attr("fill", o => (("parent" in o) ? ds.secondaryColor : ds.color))
                    .attr("stroke", "black")
                    .attr("stroke-width", 1)
                    .on("click", self.selectCallback);
                g.append("text")
                    .attr("class", (ds.cssClass !== null)
                          ? ("aeon-entity " + ds.cssClass)
                          : "aeon-entity")
                    .attr("clip-path", o => "url(#clip-" + ds.id + "-" + o.id + ")")
                    .attr("dy", "0.5em")
                    .attr("aeon:order", o => o.start)
                    .html(o => o.title)
                    .on("click", self.selectCallback);
                g.append("image")
                    .attr("class", (ds.cssClass !== null)
                          ? ("aeon-entity " + ds.cssClass)
                          : "aeon-entity")
                    .attr("clip-path", o => "url(#clip-" + ds.id + "-" + o.id + ")")
                    .attr("href", o => o.image)
                    .attr("width", self.entityHeight - 2 * IMAGE_MARGIN)
                    .attr("height", self.entityHeight - 2 * IMAGE_MARGIN)
                    .attr("aeon:order", o => o.start)
                    .on("click", self.selectCallback);
                var cp = defsData.enter().append("clipPath")
                    .attr("id", o => "clip-" + ds.id + "-" + o.id);
                cp.filter(o => (o.start != o.end)).append("rect")
                    .attr("height", o => (o.size * self.entityHeight));
                cp.filter(o => (o.start == o.end)).append("path");
            },
            transform: function(self, ds, T) {
                var minBoxWidth = self.deformOnZoomOut ? self.entityHeight : 0;
                var momentaryEntityWidth = Math.min(T.kx * MOMENTARY_ENTITY_VISUAL_TIME, MOMENTARY_ENTITY_MAX_WIDTH);
                ds.svgGroup.selectAll("rect").data(ds.data.filter(o => (o.start != o.end)))
                    .attr("x", o => (T.kx * o.start + T.x
                                     - ((o.end === null) ? 0 : Math.max(0, (minBoxWidth - T.kx * (o.end - o.start)) / 2))))
                    .attr("y", o => (self.totalHeight - (o.slot + o.size) * self.entityHeight + T.y))
                    .attr("rx", o => (o.end === null) ? 0 : Math.max(0, (minBoxWidth - T.kx * (o.end - o.start)) / 2))
                    .attr("ry", o => (o.end === null) ? 0 : Math.max(0, (minBoxWidth - T.kx * (o.end - o.start)) / 2))
                    .attr("width", o => ((o.end === null) ? T.kx * (self.maxEndTime - o.start)
                                         : Math.max(minBoxWidth, T.kx * (o.end - o.start))));
                ds.svgGroup.selectAll("path").data(ds.data.filter(o => (o.start == o.end)))
                    .attr("d", o => ("M " + (T.kx * o.start + T.x) + " "
                                     + ((self.totalHeight - (o.slot + o.size) * self.entityHeight + T.y))
                                     + " h " + (momentaryEntityWidth)
                                     + " l " + (o.size * self.entityHeight / 2) + " " + (o.size * self.entityHeight / 2)
                                     + " l " + (-o.size * self.entityHeight / 2) + " " + (o.size * self.entityHeight / 2)
                                     + " h " + (-momentaryEntityWidth) + " z"));
                ds.svgGroup.selectAll("text").data(ds.data)
                    .attr("x", o => (Math.max(self.entityHeight, T.kx * o.start + T.x + self.entityHeight)
                                     - ((o.end === null || o.end == o.start) ? 0
                                        : Math.max(0, (minBoxWidth - T.kx * (o.end - o.start)) / 2))))
                    .attr("y", o => (self.totalHeight - (o.slot + o.size - 0.5) * self.entityHeight + T.y));
                ds.svgGroup.selectAll("image").data(ds.data)
                    .attr("x", o => (Math.max(IMAGE_MARGIN, T.kx * o.start + T.x + IMAGE_MARGIN)
                                     - ((o.end === null || o.end == o.start) ? 0
                                        : Math.max(0, (minBoxWidth - T.kx * (o.end - o.start)) / 2))))
                    .attr("y", o => (self.totalHeight - (o.slot + o.size) * self.entityHeight + T.y + IMAGE_MARGIN));
                ds.defGroup.selectAll("rect").data(ds.data.filter(o => (o.start != o.end)))
                    .attr("x", o => ((T.kx * o.start + T.x)
                                     - ((o.end === null) ? 0 : Math.max(0, (minBoxWidth - T.kx * (o.end - o.start)) / 2))))
                    .attr("y", o => (self.totalHeight - (o.slot + o.size) * self.entityHeight + T.y))
                    .attr("rx", o => (o.end === null) ? 0 : Math.max(0, (minBoxWidth - T.kx * (o.end - o.start)) / 2))
                    .attr("ry", o => (o.end === null) ? 0 : Math.max(0, (minBoxWidth - T.kx * (o.end - o.start)) / 2))
                    .attr("width", o => ((o.end === null) ? T.kx * (self.maxEndTime - o.start)
                                         : Math.max(minBoxWidth, T.kx * (o.end - o.start))));
                ds.defGroup.selectAll("path").data(ds.data.filter(o => (o.start == o.end)))
                    .attr("d", o => ("M " + (T.kx * o.start + T.x) + " "
                                     + ((self.totalHeight - (o.slot + o.size) * self.entityHeight + T.y))
                                     + " h " + (momentaryEntityWidth)
                                     + " l " + (o.size * self.entityHeight / 2) + " " + (o.size * self.entityHeight / 2)
                                     + " l " + (-o.size * self.entityHeight / 2) + " " + (o.size * self.entityHeight / 2)
                                     + " h " + (-momentaryEntityWidth) + " z"));
            },
        },
        {
            name: "sub-box-exterior",
            spacePerParent: 1,
            spacePerChild: 2,
            render: function(self, ds, yScaleInfo) {
                ds.data = ds.data.sort((a, b) => ((("parent" in a) ? 1 : 0) - (("parent" in b) ? 1 : 0)))
                var mainData = ds.svgGroup.selectAll("g").data(ds.data);
                var lineData = ds.svgGroup.selectAll("line").data(ds.data.filter(o => ("parent" in o)));
                var defsData = ds.defGroup.selectAll("clipPath").data(ds.data);
                lineData.enter().append("line")
                    .attr("stroke", "black")
                    .attr("stroke-width", 1);
                var g = mainData.enter().append("g");
                g.filter(o => (o.start != o.end)).append("rect")
                    .attr("class", (ds.cssClass !== null)
                          ? ("aeon-entity " + ds.cssClass)
                          : "aeon-entity")
                    .attr("height", self.entityHeight)
                    .attr("aeon:order", o => o.start)
                    .attr("fill", o => (("parent" in o) ? ds.secondaryColor : ds.color))
                    .attr("stroke", "black")
                    .attr("stroke-width", 1)
                    .on("click", self.selectCallback);
                g.filter(o => (o.start == o.end)).append("path")
                    .attr("class", (ds.cssClass !== null)
                          ? ("aeon-entity " + ds.cssClass)
                          : "aeon-entity")
                    .attr("aeon:order", o => o.start)
                    .attr("fill", o => (("parent" in o) ? ds.secondaryColor : ds.color))
                    .attr("stroke", "black")
                    .attr("stroke-width", 1)
                    .on("click", self.selectCallback);
                g.append("text")
                    .attr("class", (ds.cssClass !== null)
                          ? ("aeon-entity " + ds.cssClass)
                          : "aeon-entity")
                    .attr("clip-path", o => "url(#clip-" + ds.id + "-" + o.id + ")")
                    .attr("dy", "0.5em")
                    .attr("aeon:order", o => o.start)
                    .html(o => o.title)
                    .on("click", self.selectCallback);
                g.append("image")
                    .attr("class", (ds.cssClass !== null)
                          ? ("aeon-entity " + ds.cssClass)
                          : "aeon-entity")
                    .attr("clip-path", o => "url(#clip-" + ds.id + "-" + o.id + ")")
                    .attr("href", o => o.image)
                    .attr("width", self.entityHeight - 2 * IMAGE_MARGIN)
                    .attr("height", self.entityHeight - 2 * IMAGE_MARGIN)
                    .attr("aeon:order", o => o.start)
                    .on("click", self.selectCallback);
                var cp = defsData.enter().append("clipPath")
                    .attr("id", o => "clip-" + ds.id + "-" + o.id);
                cp.filter(o => (o.start != o.end)).append("rect")
                    .attr("height", self.entityHeight);
                cp.filter(o => (o.start == o.end)).append("path");
            },
            transform: function(self, ds, T) {
                var minBoxWidth = self.deformOnZoomOut ? self.entityHeight : 0;
                var momentaryEntityWidth = Math.min(T.kx * MOMENTARY_ENTITY_VISUAL_TIME, MOMENTARY_ENTITY_MAX_WIDTH);
                var pos = function(o) { return ("parent" in o) ? 3 : 1 };
                ds.svgGroup.selectAll("line").data(ds.data.filter(o => ("parent" in o)))
                    .attr("x1", o => (T.kx * o.start + T.x))
                    .attr("y1", o => (self.totalHeight - (o.slot + 2) * self.entityHeight + T.y))
                    .attr("x2", o => (T.kx * o.start + T.x))
                    .attr("y2", o => (self.totalHeight - (o.parent.slot + 1) * self.entityHeight + T.y));
                ds.svgGroup.selectAll("rect").data(ds.data.filter(o => (o.start != o.end)))
                    .attr("x", o => (T.kx * o.start + T.x
                                     - ((o.end === null) ? 0 : Math.max(0, (minBoxWidth - T.kx * (o.end - o.start)) / 2))))
                    .attr("y", o => (self.totalHeight - (o.slot + pos(o)) * self.entityHeight + T.y))
                    .attr("rx", o => (o.end === null) ? 0 : Math.max(0, (minBoxWidth - T.kx * (o.end - o.start)) / 2))
                    .attr("ry", o => (o.end === null) ? 0 : Math.max(0, (minBoxWidth - T.kx * (o.end - o.start)) / 2))
                    .attr("width", o => ((o.end === null) ? T.kx * (self.maxEndTime - o.start)
                                         : Math.max(minBoxWidth, T.kx * (o.end - o.start))));
                ds.svgGroup.selectAll("path").data(ds.data.filter(o => (o.start == o.end)))
                    .attr("d", o => ("M " + (T.kx * o.start + T.x) + " "
                                     + ((self.totalHeight - (o.slot + pos(o)) * self.entityHeight + T.y))
                                     + " h " + (momentaryEntityWidth)
                                     + " l " + (self.entityHeight / 2) + " " + (self.entityHeight / 2)
                                     + " l " + (-self.entityHeight / 2) + " " + (self.entityHeight / 2)
                                     + " h " + (-momentaryEntityWidth) + " z"));
                ds.svgGroup.selectAll("text").data(ds.data)
                    .attr("x", o => (Math.max(self.entityHeight, T.kx * o.start + T.x + self.entityHeight)
                                     - ((o.end === null || o.end == o.start) ? 0
                                        : Math.max(0, (minBoxWidth - T.kx * (o.end - o.start)) / 2))))
                    .attr("y", o => (self.totalHeight - (o.slot + pos(o) - 0.5) * self.entityHeight + T.y));
                ds.svgGroup.selectAll("image").data(ds.data)
                    .attr("x", o => (Math.max(IMAGE_MARGIN, T.kx * o.start + T.x + IMAGE_MARGIN)
                                     - ((o.end === null || o.end == o.start) ? 0
                                        : Math.max(0, (minBoxWidth - T.kx * (o.end - o.start)) / 2))))
                    .attr("y", o => (self.totalHeight - (o.slot + pos(o)) * self.entityHeight + T.y + IMAGE_MARGIN));
                ds.defGroup.selectAll("rect").data(ds.data.filter(o => (o.start != o.end)))
                    .attr("x", o => ((T.kx * o.start + T.x)
                                     - ((o.end === null) ? 0 : Math.max(0, (minBoxWidth - T.kx * (o.end - o.start)) / 2))))
                    .attr("y", o => (self.totalHeight - (o.slot + pos(o)) * self.entityHeight + T.y))
                    .attr("rx", o => (o.end === null) ? 0 : Math.max(0, (minBoxWidth - T.kx * (o.end - o.start)) / 2))
                    .attr("ry", o => (o.end === null) ? 0 : Math.max(0, (minBoxWidth - T.kx * (o.end - o.start)) / 2))
                    .attr("width", o => ((o.end === null) ? T.kx * (self.maxEndTime - o.start)
                                         : Math.max(minBoxWidth, T.kx * (o.end - o.start))));
                ds.defGroup.selectAll("path").data(ds.data.filter(o => (o.start == o.end)))
                    .attr("d", o => ("M " + (T.kx * o.start + T.x) + " "
                                     + ((self.totalHeight - (o.slot + pos(o)) * self.entityHeight + T.y))
                                     + " h " + (momentaryEntityWidth)
                                     + " l " + (self.entityHeight / 2) + " " + (self.entityHeight / 2)
                                     + " l " + (-self.entityHeight / 2) + " " + (self.entityHeight / 2)
                                     + " h " + (-momentaryEntityWidth) + " z"));
            },
        },
        {
            name: "parent-bar",
            spacePerParent: 1,
            spacePerChild: 0,
            render: function(self, ds, yScaleInfo) {
                var mainData = ds.svgGroup.selectAll("g").data(ds.data);
                var defsData = ds.defGroup.selectAll("clipPath").data(ds.data.filter(o => ("parent" in o)));
                var g = mainData.enter().append("g");
                var pg = g.filter(o => !("parent" in o));
                var cg = g.filter(o => ("parent" in o));
                // Parent objects
                pg.append("path")
                    .attr("class", (ds.cssClass !== null)
                          ? ("aeon-entity " + ds.cssClass)
                          : "aeon-entity")
                    .attr("stroke", "black")
                    .attr("stroke-width", 1)
                    .on("click", self.selectCallback);
                // Child objects
                cg.filter(o => (o.start != o.end)).append("rect")
                    .attr("class", (ds.cssClass !== null)
                          ? ("aeon-child-entity " + ds.cssClass)
                          : "aeon-child-entity")
                    .attr("height", self.entityHeight)
                    .attr("aeon:order", o => o.start)
                    .attr("fill", ds.color)
                    .attr("stroke", "black")
                    .attr("stroke-width", 1)
                    .on("click", self.selectCallback);
                cg.filter(o => (o.start == o.end)).append("path")
                    .attr("class", (ds.cssClass !== null)
                          ? ("aeon-child-entity " + ds.cssClass)
                          : "aeon-child-entity")
                    .attr("aeon:order", o => o.start)
                    .attr("fill", ds.color)
                    .attr("stroke", "black")
                    .attr("stroke-width", 1)
                    .on("click", self.selectCallback);
                cg.append("text")
                    .attr("class", (ds.cssClass !== null)
                          ? ("aeon-child-entity " + ds.cssClass)
                          : "aeon-child-entity")
                    .attr("clip-path", o => "url(#clip-" + ds.id + "-" + o.id + ")")
                    .attr("dy", "0.5em")
                    .attr("aeon:order", o => o.start)
                    .html(o => o.parent.title)
                    .on("click", self.selectCallback);
                cg.append("image")
                    .attr("class", (ds.cssClass !== null)
                          ? ("aeon-child-entity " + ds.cssClass)
                          : "aeon-child-entity")
                    .attr("clip-path", o => "url(#clip-" + ds.id + "-" + o.id + ")")
                    .attr("href", o => o.parent.image)
                    .attr("width", self.entityHeight - 2 * IMAGE_MARGIN)
                    .attr("height", self.entityHeight - 2 * IMAGE_MARGIN)
                    .attr("aeon:order", o => o.start)
                    .on("click", self.selectCallback);
                var cp = defsData.enter().append("clipPath")
                    .attr("id", o => "clip-" + ds.id + "-" + o.id);
                cp.filter(o => (o.start != o.end)).append("rect")
                    .attr("height", self.entityHeight);
                cp.filter(o => (o.start == o.end)).append("path");
            },
            transform: function(self, ds, T) {
                var pd = ds.data.filter(o => !("parent" in o));
                var cd = ds.data.filter(o => ("parent" in o));
                // Parent objects
                ds.svgGroup.selectAll("path.aeon-entity").data(pd)
                    .attr("d", o => ("M " + (T.kx * o.start + T.x) + " "
                                     + ((self.totalHeight - (o.slot + 1/2) * self.entityHeight + T.y)) + " "
                                     + "h " + ((o.end === null) ? T.kx * (self.maxEndTime - o.start)
                                               : T.kx * (o.end - o.start)) + " "
                                     + "M " + (T.kx * o.start + T.x) + " "
                                     + ((self.totalHeight - (o.slot + 1) * self.entityHeight + T.y)) + " "
                                     + "v " + self.entityHeight + " "
                                     + ((o.end === null) ? "" :
                                        (
                                            "M " + (T.kx * o.end + T.x) + " "
                                                + ((self.totalHeight - (o.slot + 1) * self.entityHeight + T.y)) + " "
                                                + "v " + self.entityHeight
                                        )
                                       )));
                // Child objects
                var momentaryEntityWidth = Math.min(T.kx * MOMENTARY_ENTITY_VISUAL_TIME, MOMENTARY_ENTITY_MAX_WIDTH);
                ds.svgGroup.selectAll("rect").data(cd.filter(o => (o.start != o.end)))
                    .attr("x", o => (T.kx * o.start + T.x
                                     - ((o.end === null) ? 0 : Math.max(0, (-T.kx * (o.end - o.start)) / 2))))
                    .attr("y", o => (self.totalHeight - (o.parent.slot + 1) * self.entityHeight + T.y))
                    .attr("width", o => ((o.end === null) ? T.kx * (self.maxEndTime - o.start)
                                         : Math.max(0, T.kx * (o.end - o.start))));
                ds.svgGroup.selectAll("path.aeon-child-entity").data(cd.filter(o => (o.start == o.end)))
                    .attr("d", o => ("M " + (T.kx * o.start + T.x) + " "
                                     + ((self.totalHeight - (o.parent.slot + 1) * self.entityHeight + T.y))
                                     + " h " + (momentaryEntityWidth)
                                     + " l " + (self.entityHeight / 2) + " " + (self.entityHeight / 2)
                                     + " l " + (-self.entityHeight / 2) + " " + (self.entityHeight / 2)
                                     + " h " + (-momentaryEntityWidth) + " z"));
                ds.svgGroup.selectAll("text").data(cd)
                    .attr("x", o => (Math.max(self.entityHeight, T.kx * o.start + T.x + self.entityHeight)
                                     - ((o.end === null || o.end == o.start) ? 0
                                        : Math.max(0, (-T.kx * (o.end - o.start)) / 2))))
                    .attr("y", o => (self.totalHeight - (o.parent.slot + 0.5) * self.entityHeight + T.y));
                ds.svgGroup.selectAll("image").data(cd)
                    .attr("x", o => (Math.max(IMAGE_MARGIN, T.kx * o.start + T.x + IMAGE_MARGIN)
                                     - ((o.end === null || o.end == o.start) ? 0
                                        : Math.max(0, (-T.kx * (o.end - o.start)) / 2))))
                    .attr("y", o => (self.totalHeight - (o.parent.slot + 1) * self.entityHeight + T.y + IMAGE_MARGIN));
                ds.defGroup.selectAll("rect").data(cd.filter(o => (o.start != o.end)))
                    .attr("x", o => ((T.kx * o.start + T.x)
                                     - ((o.end === null) ? 0 : Math.max(0, (-T.kx * (o.end - o.start)) / 2))))
                    .attr("y", o => (self.totalHeight - (o.parent.slot + 1) * self.entityHeight + T.y))
                    .attr("width", o => ((o.end === null) ? T.kx * (self.maxEndTime - o.start)
                                         : Math.max(0, T.kx * (o.end - o.start))));
                ds.defGroup.selectAll("path").data(cd.filter(o => (o.start == o.end)))
                    .attr("d", o => ("M " + (T.kx * o.start + T.x) + " "
                                     + ((self.totalHeight - (o.parent.slot + 1) * self.entityHeight + T.y))
                                     + " h " + (momentaryEntityWidth)
                                     + " l " + (self.entityHeight / 2) + " " + (self.entityHeight / 2)
                                     + " l " + (-self.entityHeight / 2) + " " + (self.entityHeight / 2)
                                     + " h " + (-momentaryEntityWidth) + " z"));
            },
        },
        {
            name: "highlight-children",
            spacePerParent: 1,
            spacePerChild: 0,
            render: function(self, ds, yScaleInfo) {
                var pd = ds.data.filter(o => !("parent" in o));
                var cd = ds.data.filter(o => ("parent" in o));
                var defsData = ds.defGroup.selectAll("clipPath").data(ds.data.filter(o => !("parent" in o)));
                // Parent object containers
                ds.svgGroup.selectAll("rect.aeon-entity").data(pd).enter().append("rect")
                    .attr("class", (ds.cssClass !== null)
                          ? ("aeon-entity " + ds.cssClass)
                          : "aeon-entity")
                    .attr("height", self.entityHeight - 2)
                    .attr("aeon:order", o => o.start)
                    .attr("fill", ds.secondaryColor)
                    .attr("stroke", "none")
                    .on("click", self.selectCallback);
                // Child object subregions
                ds.svgGroup.selectAll("rect.aeon-child-entity").data(cd.filter(o => (o.start != o.end))).enter()
                    .append("rect")
                    .attr("class", (ds.cssClass !== null)
                          ? ("aeon-child-entity " + ds.cssClass)
                          : "aeon-child-entity")
                    .attr("height", self.entityHeight - 2)
                    .attr("aeon:order", o => o.start)
                    .attr("fill", ds.color)
                    .attr("stroke", "none")
                    .on("click", self.selectCallback);
                ds.svgGroup.selectAll("path").data(cd.filter(o => (o.start == o.end))).enter().append("path")
                    .attr("class", (ds.cssClass !== null)
                          ? ("aeon-child-entity " + ds.cssClass)
                          : "aeon-child-entity")
                    .attr("aeon:order", o => o.start)
                    .attr("fill", ds.color)
                    .attr("stroke", "none")
                    .on("click", self.selectCallback);
                // Parent object text and icons
                ds.svgGroup.selectAll("text").data(pd).enter().append("text")
                    .attr("class", (ds.cssClass !== null)
                          ? ("aeon-child-entity " + ds.cssClass)
                          : "aeon-child-entity")
                    .attr("clip-path", o => "url(#clip-" + ds.id + "-" + o.id + ")")
                    .attr("dy", "0.5em")
                    .attr("aeon:order", o => o.start)
                    .html(o => o.title)
                    .on("click", self.selectCallback);
                ds.svgGroup.selectAll("image").data(pd).enter().append("image")
                    .attr("class", (ds.cssClass !== null)
                          ? ("aeon-child-entity " + ds.cssClass)
                          : "aeon-child-entity")
                    .attr("clip-path", o => "url(#clip-" + ds.id + "-" + o.id + ")")
                    .attr("href", o => o.image)
                    .attr("width", self.entityHeight - 2 * IMAGE_MARGIN)
                    .attr("height", self.entityHeight - 2 * IMAGE_MARGIN)
                    .attr("aeon:order", o => o.start)
                    .on("click", self.selectCallback);
                // Parent object clipping paths
                var cp = defsData.enter().append("clipPath")
                    .attr("id", o => "clip-" + ds.id + "-" + o.id);
                cp.filter(o => (o.start != o.end)).append("rect")
                    .attr("height", self.entityHeight);
                cp.filter(o => (o.start == o.end)).append("path");
            },
            transform: function(self, ds, T) {
                var pd = ds.data.filter(o => !("parent" in o));
                var cd = ds.data.filter(o => ("parent" in o));
                var momentaryEntityWidth = Math.min(T.kx * MOMENTARY_ENTITY_VISUAL_TIME, MOMENTARY_ENTITY_MAX_WIDTH);
                // Parent object containers
                ds.svgGroup.selectAll("rect.aeon-entity").data(pd)
                    .attr("x", o => (T.kx * o.start + T.x
                                     - ((o.end === null) ? 0 : Math.max(0, (-T.kx * (o.end - o.start)) / 2))))
                    .attr("y", o => (self.totalHeight - (o.slot + 1) * self.entityHeight + T.y + 1))
                    .attr("width", o => ((o.end === null) ? T.kx * (self.maxEndTime - o.start)
                                         : Math.max(0, T.kx * (o.end - o.start))));
                // Child object subregions
                ds.svgGroup.selectAll("rect.aeon-child-entity").data(cd.filter(o => (o.start != o.end)))
                    .attr("x", o => (T.kx * o.start + T.x
                                     - ((o.end === null) ? 0 : Math.max(0, (-T.kx * (o.end - o.start)) / 2))))
                    .attr("y", o => (self.totalHeight - (o.parent.slot + 1) * self.entityHeight + T.y + 1))
                    .attr("width", o => ((o.end === null) ? T.kx * (self.maxEndTime - o.start)
                                         : Math.max(0, T.kx * (o.end - o.start))));
                ds.svgGroup.selectAll("path").data(cd.filter(o => (o.start == o.end)))
                    .attr("d", o => ("M " + (T.kx * o.start + T.x) + " "
                                     + ((self.totalHeight - (o.parent.slot + 1) * self.entityHeight + T.y + 1))
                                     + " h " + (momentaryEntityWidth)
                                     + " l " + ((self.entityHeight - 2) / 2) + " " + ((self.entityHeight - 2) / 2)
                                     + " l " + (-(self.entityHeight - 2) / 2) + " " + ((self.entityHeight - 2) / 2)
                                     + " h " + (-momentaryEntityWidth) + " z"));
                // Parent object text and icons
                ds.svgGroup.selectAll("text").data(pd)
                    .attr("x", o => (Math.max(self.entityHeight, T.kx * o.start + T.x + self.entityHeight)
                                     - ((o.end === null || o.end == o.start) ? 0
                                        : Math.max(0, (-T.kx * (o.end - o.start)) / 2))))
                    .attr("y", o => (self.totalHeight - (o.slot + 0.5) * self.entityHeight + T.y + 1));
                ds.svgGroup.selectAll("image").data(pd)
                    .attr("x", o => (Math.max(IMAGE_MARGIN, T.kx * o.start + T.x + IMAGE_MARGIN)
                                     - ((o.end === null || o.end == o.start) ? 0
                                        : Math.max(0, (-T.kx * (o.end - o.start)) / 2))))
                    .attr("y", o => (self.totalHeight - (o.slot + 1) * self.entityHeight + T.y + IMAGE_MARGIN + 1));
                // Parent object clipping paths
                ds.defGroup.selectAll("rect").data(pd.filter(o => (o.start != o.end)))
                    .attr("x", o => ((T.kx * o.start + T.x)
                                     - ((o.end === null) ? 0 : Math.max(0, (-T.kx * (o.end - o.start)) / 2))))
                    .attr("y", o => (self.totalHeight - (o.slot + 1) * self.entityHeight + T.y + 1))
                    .attr("width", o => ((o.end === null) ? T.kx * (self.maxEndTime - o.start)
                                         : Math.max(0, T.kx * (o.end - o.start))));
                ds.defGroup.selectAll("path").data(pd.filter(o => (o.start == o.end)))
                    .attr("d", o => ("M " + (T.kx * o.start + T.x) + " "
                                     + ((self.totalHeight - (o.slot + 1) * self.entityHeight + T.y + 1))
                                     + " h " + (momentaryEntityWidth)
                                     + " l " + ((self.entityHeight - 2) / 2) + " " + ((self.entityHeight - 2) / 2)
                                     + " l " + (-(self.entityHeight - 2) / 2) + " " + ((self.entityHeight - 2) / 2)
                                     + " h " + (-momentaryEntityWidth) + " z"));
            },
        },
    ];
    const DEFAULT_STYLE = "sub-box-interior";

    function EntityRenderer(selectCallback) {
        this.boundaries = [];
        this.entityHeight = DEFAULT_ENTITY_HEIGHT;
        this.totalHeight = 0;
        this.deformOnZoomOut = false;
        this.selectCallback = selectCallback;

        this.findConsecutivelyFreeSlots = function(slots, size) {
            var slot = 0, streak = 0;
            for (var i = 0; i < slots.length; i++) {
                if (slots[i]) {
                    // Slot busy.
                    streak = 0;
                    slot = i + 1;
                } else {
                    // Slot free.
                    streak++;
                    if (streak == size) {
                        return slot;
                    }
                }
            }
            return slot;
        }

        this.fillSlots = function(slots, start, size, value) {
            for (var i = start; i < start + size; i++) {
                if (i >= slots.length) {
                    slots.push(value);
                } else {
                    slots[i] = value;
                }
            }
        }

        this.shiftObjectRecursive = function(obj, n) {
            obj.slot += n;
            for (let bnd of this.boundaries.filter(
                e => (e.type === "<" && "parent" in e.obj && e.obj.parent === obj))) {
                this.shiftObjectRecursive(bnd.obj, n);
            }
        }

        this.stackEntities = function(bnd) {
            var slots = new Array(0);
            for (var i = 0; i < bnd.length; i++) {
                if (bnd[i].type === "<") {
                    // Place children and determine how much space they need.
                    var size;
                    var children = this.boundaries.filter(e => ("parent" in e.obj && e.obj.parent === bnd[i].obj));
                    if (children.length > 0) {
                        size = bnd[i].dataset.entityStyle.spacePerParent + this.stackEntities(children);
                    } else {
                        size = bnd[i].dataset.entityStyle.spacePerChild;
                    }
                    // Find 'size' consecutively free slots.
                    var slot = this.findConsecutivelyFreeSlots(slots, size);
                    this.fillSlots(slots, slot, size, true);
                    bnd[i].obj.slot = 0;
                    bnd[i].obj.size = size;
                    this.shiftObjectRecursive(bnd[i].obj, slot);
                } else {
                    this.fillSlots(slots, bnd[i].obj.slot, bnd[i].obj.size, false);
                }
            }
            return slots.length;
        }

        this.updateStackOrder = function() {
            var len = this.stackEntities(this.boundaries.filter(e => !("parent" in e.obj)));
            this.totalHeight = len * this.entityHeight;
        }
    }

    /////////////////////////////////////
    // Specific methods for this renderer

    EntityRenderer.prototype.getTotalHeight = function() {
        return this.totalHeight;
    }

    EntityRenderer.prototype.setEntityHeight = function(entityHeight) {
        this.entityHeight = entityHeight;
        this.updateStackOrder();
    }

    EntityRenderer.prototype.setDeformOnZoomOut = function(deformEnabled) {
        this.deformOnZoomOut = !!deformEnabled;
    }

    EntityRenderer.prototype.setMaxTimeRange = function(maxStartTime, maxEndTime) {
        this.maxStartTime = maxStartTime;
        this.maxEndTime = maxEndTime;
    }

    EntityRenderer.prototype.getStyles = function() {
        return styles;
    }

    EntityRenderer.prototype.getDefaultStyle = function() {
        return styles.find(s => (s.name == DEFAULT_STYLE));
    }

    EntityRenderer.prototype.setStyle = function(dataset, style) {
        var styleObject = styles.find(s => (s.name == style));
        if (styleObject === undefined) {
            console.warn("Unknown entity style '" + style + "'");
            return;
        }
        dataset.entityStyle = styleObject;
        this.updateStackOrder();
    }

    ///////////////////////
    // Generic renderer API

    EntityRenderer.prototype.addDataset = function(ds) {
        for (var i = 0; i < ds.data.length; i++) {
            var start, end;
            if (ds.data[i].start == ds.data[i].end) {
                start = ds.data[i].start;
                end = ds.data[i].start + MOMENTARY_ENTITY_VISUAL_TIME;
            } else {
                start = ds.data[i].start;
                end = ds.data[i].end;
            }
            this.boundaries.push({dataset: ds, type: "<", time: start, obj: ds.data[i]});
            if (end !== null) {
                this.boundaries.push({dataset: ds, type: ">", time: end, obj: ds.data[i]});
            }
        }
        this.boundaries.sort((a, b) => (a.time - b.time));
        this.updateStackOrder();
    }

    EntityRenderer.prototype.removeDataset = function(ds) {
        this.boundaries = this.boundaries.filter(e => e.dataset != ds);
        this.updateStackOrder();
    }

    EntityRenderer.prototype.render = function(ds, yScaleInfo) {
        ds.svgGroup.selectAll("*").remove();
        ds.defGroup.selectAll("*").remove();
        ds.entityStyle.render(this, ds, yScaleInfo);
    }

    EntityRenderer.prototype.transform = function(ds, T) {
        ds.entityStyle.transform(this, ds, T);
    }

    return {
        EntityRenderer: EntityRenderer,
    };
});
